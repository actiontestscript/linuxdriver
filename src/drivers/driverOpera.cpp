#include "../../include/drivers/driverOpera.hpp"

namespace ld{
    Opera::Opera(){
        m_session = Session::getInstance();
        m_driverName = "opera";
        m_driverProcessName = "operadriver";
        m_browserName = "opera";
        m_browserPath = "/usr/bin/opera";
        m_defaultPort = 9710;       //default driver 9515
        m_port = m_defaultPort;
        m_localListIps = Network::getListIpsLocal();
        m_downloadUrl +="opera/";
        m_iconBrowserBase64 = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAB1FBMVEVHcEzaOi24MSblPS/pPjDpPjDLNirpPjDpPjDpPjCbKSDhPC6NJR2tLiOGIxvfOy6VJx7AMyflPS+vLiTeOy3jPC6jKyG/LiLpPjDdOi3jPC7pPjC/LiK/LiKGIxuXKB+JJByVJx6xLyTJNSnINSmvLiTXOSzWOSy+MifINSm3LCHALiK6LSHALiK/LiKGIBjALiJwGxTALiKqKR6MIhm+LiK8LSKkKB2/LiKBHxe7LSHBLiLALiK/LiKBHxdzGxSbJRzALiKtKh+SIxrBLiLALiJuGhSBHxfALiKuKh/ALiLBLiKmKB6/LiKRIxrALiK/LiJ3HRXBLiLBLiLAMye2MCWeJhy/LiLBLiLALiK6LSHALiK/LiKzKyDALiLBLyLALiLBLiK4LCF8Hha/LiLALiKmKB6xKyDBLiLALiJ/HxeWJBufJhzAMyfALiK/LiK/LiK/LiLpPjC/LiLoPS/UNinALiLnPS/mPS/DLyPFMCTdOSzCLyPfOizkPC7WNynWNinIMSXGMCTgOi3HMSTTNSjEMCPaOCvlPC7OMyfiOy3JMiXNMybMMybeOizYNyrmPC/ZOCrVNinBLiLbOCvQNCfRNSjWNyrhOy3LMibXNyrjOy4bdhGFAAAAcnRSTlMAawxUzqhr3bb3BFQEDARYBBVYDFRUDPCpWFjP5f0ICAgIN1paN1paNxVPymG54R+wDJtpDntcPvsWdI5v9RAOMstsEZi8AwhuQsSkI/oMtuMBd5Y2FR3+dWhHwfQpWoG6kzEE+NlGO4ZxJTUbN/7O7NyaJjA9AAAB5ElEQVQ4y5VTZ0MTURBMNMkl0rv03mwoYkFBxa6ADbA3LCggqDvHXUJC6N1eKX+WSUjI3YMv3JedvZm3+94Wh2M3X3ldrdNZM1S6M5te4PWJmH5dfN7Mfdv5HE1kbDEAGMtrpmh7FTrLLRJexdv+wYG+3qeBPyLuPVY+wyUyHSp+8Trq3b+JWV1c9Qk+hfxS6MGjuH/tCkZEXIkYjG9OoStx4tIRBJkl7iaJyAiKbllyHsCcKRK7aTLv/+M3rttufRDjItrma9MYYAKtTTbBIYzqIplR7KVgEkft7z53GtMi3ggsY/38QItSmTb8EvFFql7BAN+AU4qgEV9IdBNV0v4EGhTBCUySqCHy0I4D5xXBRYyScMYEE8AxRXB8S1BF+x1oVgRnMEOilqiaNghcUARn8ZVEHVEJn7kAnFQEhyP98pXHCzWFy3b+ah7GYoVy5Ed7deeuTXADAZa6IIpT2aywgcc2QXskg5a+6WQzxD88sfIdnSG/SE7c3c9uBPDcIriNecvAOHILOdFG3rOtH/fwlyOXlTjQQ0XQKHoVcx8WL3JoM6w5c5llZebN+w/E714a/3VxpyiFydZEn18e/vzx09xsWLSk7auVms/VM9eXFrh6ack7r2dJdZXHU1lRtquN3wA4fZTdJgG0KwAAAABJRU5ErkJggg==";
        m_webDriverOffsetX = 0;
        m_webDriverOffsetY = 0;
        m_webDriverHeadlessOffsetX = 0;
        m_webDriverHeadlessOffsetY = 0;
        /*
        m_webDriverOffsetX = -2;
        m_webDriverOffsetY = 28;
        m_webDriverHeadlessOffsetX = 2;
        m_webDriverHeadlessOffsetY = 40;
*/
    }


    std::string Opera::getBrowserVersion(const bool& fullVersion) {
        std::string version ="not found";
        std::string result{};

        std::string cmd = m_browserName + " --version";
        if (ld::Utils::exec(cmd.c_str(), result)) {
            std::regex pattern;
            if(fullVersion) pattern = R"(^(\d+\.\d+\.\d+\.\d+).*)";
            else pattern = R"(^(\d+)\..*)";       /*> pattern use to find major version */
            std::smatch matches;
                if(std::regex_search(result, matches, pattern)){
                    version = matches[1].str();
                    int v = std::stoi(version);
                    v += 14;
                    version = std::to_string(v);
                }
        }
        return version;
    }

    std::string Opera::getWebDriverVersion(const bool& fullVersion) {
        std::string result{};
        std::string currentPath = ld::Config::currentPath;
        currentPath += DIRSEPARATOR;
        std::string driverPath = currentPath + m_driverProcessName;
        if(!ld::Utils::fileExist(driverPath)){return "not found"; }

        std::string cmd = driverPath + " --version";
        if(ld::Utils::exec(cmd.c_str(), result)){
            std::regex pattern;
            if(fullVersion) pattern = R"(^OperaDriver\s+(\d+\.\d+\.\d+\.\d+).*)";
//            if(fullVersion) pattern = R"(^ChromeDriver\s+(\d+\.\d+\.\d+\.\d+).*)";
//            else pattern = R"(^ChromeDriver\s+(\d+)\..*)";       /*> pattern use to find major version */
            else pattern = R"(^OperaDriver\s+(\d+)\..*)";       /*> pattern use to find major version */
            std::smatch matches;
            if(std::regex_search(result, matches, pattern)){
                return matches[1].str();
            }
        }
        return "not found";
    }

    bool Opera::downloadWebdriver() {

        std::string latest_version = getBrowserVersion(false);
        std::string result{};
        std::string currentPath = ld::Config::currentPath;
        currentPath += DIRSEPARATOR;

        std::string urlDownload = m_downloadUrl + latest_version + ".tgz";
        std::string cmdDownload = "wget " + urlDownload + " -q -O "+ currentPath + m_driverProcessName +".tgz"; // -q quiet mode, -O output
        LD_LOG_DEBUG << "cmdDownload: " << cmdDownload << std::endl;
        std::thread t1(ld::Utils::exec, cmdDownload.c_str(), std::ref(result));
        t1.join();

        result="";
        cmdDownload = "du -b "+ currentPath + m_driverProcessName +".tgz | awk '{print $1}'";
        LD_LOG_DEBUG << "check size cmd: " << cmdDownload << std::endl;
        std::regex pattern("\\n");
        ld::Utils::exec(cmdDownload.c_str(), result);
        result = std::regex_replace(result, pattern, "");
        if(result == "0"){
            if(ld::Utils::fileExist(currentPath + m_driverProcessName)) return true;
            LD_LOG_DEBUG << "Download failed driver opera check size incorrect" << std::endl;
            return false;
        }

        //std::string cmdUnzip = "unzip -jqo "+ currentPath + m_driverProcessName +".zip operadriver_linux64/operadriver";
        //std::string cmdUnTar = "tar -xzf "+ currentPath + m_driverProcessName +".tgz operadriver_linux64/operadriver  --strip-components=1";
        std::string cmdUnTar = "tar -xzf "+ currentPath + m_driverProcessName +".tgz -C"+currentPath;;
        LD_LOG_DEBUG << "cmdUnTar: " << cmdUnTar << std::endl;
        std::thread t2(ld::Utils::exec, cmdUnTar.c_str(), std::ref(result));
        t2.join();

        //delete zip file
        std::string cmdDelete = "rm -f "+ currentPath + m_driverProcessName+".tgz";
        LD_LOG_DEBUG << "cmdDelete: " << cmdDelete << std::endl;
        std::thread t3(ld::Utils::exec, cmdDelete.c_str(), std::ref(result));
        t3.join();
/*
        //mv file on current path
        std::string cmdMove = "mv -f ./"+ m_driverProcessName +" " + currentPath;
        LD_LOG_DEBUG << "cmdMove: " << cmdMove << std::endl;
        std::thread t4(ld::Utils::exec, cmdMove.c_str(), std::ref(result));
        t4.join();


        std::string cmdChmod = "chmod +x "+ currentPath + m_driverProcessName;
        LD_LOG_DEBUG << "cmdChmod: " << cmdChmod << std::endl;
        std::thread t5(ld::Utils::exec, cmdChmod.c_str(), std::ref(result));
        t5.join();
*/
        //verify if the file is downloaded
        //std::string cmdVerify = "./"+m_driverProcessName+ " --version";
        std::string cmdVerify = currentPath + m_driverProcessName+" --version";
        LD_LOG_DEBUG << "cmdVerify version : " << cmdVerify << std::endl;
        std::thread t6(ld::Utils::exec, cmdVerify.c_str(), std::ref(result));
        t6.join();
        if (getWebDriverVersion(false) == latest_version) {
            return true;
        }

        LD_LOG_WARNING << "diff version on opera and opera driver" << std::endl;
        return false;
    }

}//namespace ld

