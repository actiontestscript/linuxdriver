#include "../../../include/W3C/navigate/navigate.hpp"

namespace ld {
    Navigate::Navigate() {
        m_session = Session::getInstance();
    }

    void Navigate::navigateTo(const std::string& sessionId,const crow::request &req, crow::response& res){
        CrowJsonResponse jRes;
        SessionData::SessionInfo* sessionInfo = m_session->getSessionInfo(sessionId);
        bool noSession = (sessionInfo == nullptr || m_session->isEndedSession(sessionId));
        if(noSession){
            jRes.setMsgErrorCode("invalid_session_id","","");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return;
        }
        //Common Verifications for all Json
        std::string body = req.body;
        if(!jRes.isValidJson(body)){
            jRes.setMsgErrorCode("invalid_argument","","Is not a valid JSON string");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return;
        }

        json jsonMsgReturn;
        auto caps = CapabilitiesFactory().create<Capabilities>(sessionInfo->nameCapabilities);
        if(!caps->navigateTo( sessionId, req)){
            jRes.setMsgErrorCode("invalid_session_id","","");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return;
        }

        jsonMsgReturn["value"] = nullptr;
        res.code = 200;
        res.write(jsonMsgReturn.dump());
    }

    void Navigate::getCurrentUrl(const std::string &sessionId, crow::response &res) {
        CrowJsonResponse jRes;
        SessionData::SessionInfo* sessionInfo = m_session->getSessionInfo(sessionId);
        bool noSession = (sessionInfo == nullptr || m_session->isEndedSession(sessionId));
        if(noSession){
            jRes.setMsgErrorCode("invalid_session_id","","");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return;
        }
        json jsonMsgReturn;
        auto caps = CapabilitiesFactory().create<Capabilities>(sessionInfo->nameCapabilities);
        if(!caps->getCurrentUrl(sessionId,jsonMsgReturn)){
            jRes.setMsgErrorCode("invalid_session_id","","");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return;
        }

        res.code = 200;
        res.write(jsonMsgReturn.dump());
    }

    void Navigate::navigateBack(const std::string &sessionId, crow::response &res) {
        CrowJsonResponse jRes;
        SessionData::SessionInfo* sessionInfo = m_session->getSessionInfo(sessionId);
        bool noSession = (sessionInfo == nullptr || m_session->isEndedSession(sessionId));
        if(noSession){
            jRes.setMsgErrorCode("invalid_session_id","","");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return;
        }
        json jsonMsgReturn;
        auto caps = CapabilitiesFactory().create<Capabilities>(sessionInfo->nameCapabilities);
        if(!caps->navigateBack(sessionId)){
            jRes.setMsgErrorCode("invalid_session_id","","");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return;
        }
        jsonMsgReturn["value"] = nullptr;
        res.code = 200;
        res.write(jsonMsgReturn.dump());

    }

    void Navigate::navigateForward(const std::string &sessionId, crow::response &res) {
        CrowJsonResponse jRes;
        SessionData::SessionInfo* sessionInfo = m_session->getSessionInfo(sessionId);
        bool noSession = (sessionInfo == nullptr || m_session->isEndedSession(sessionId));
        if(noSession){
            jRes.setMsgErrorCode("invalid_session_id","","");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return;
        }
        json jsonMsgReturn;
        auto caps = CapabilitiesFactory().create<Capabilities>(sessionInfo->nameCapabilities);
        if(!caps->navigateForward(sessionId)){
            jRes.setMsgErrorCode("invalid_session_id","","");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return;
        }
        jsonMsgReturn["value"] = nullptr;
        res.code = 200;
        res.write(jsonMsgReturn.dump());
    }

}