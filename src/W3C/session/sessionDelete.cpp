#include "../../include/W3C/session/sessionDelete.hpp"
namespace ld {
    SessionDelete::SessionDelete(){
        m_session = Session::getInstance();
    }

    void SessionDelete::delSession(const std::string &sessionId, crow::response &res){
        if(!m_session->m_sessions.empty()){
            for(auto& session: m_session->m_sessions){
                if(session.id == sessionId && session.isWebDriver && !session.isEnded){
                    auto driver = DriversFactory().create<Driver>(session.webdriverProcessName);
                    if(driver != nullptr){
                        driver->isResponseExecuted=true;
                        driver->deleteDriver(res, session.webdriverPort);
                        session.isEnded = true;
                    }
                }
            }
        }

        m_session->deleteSession(sessionId);

        getJsonNullResponse(res);
    }

    void SessionDelete::deleteAllSession() {
        if(!m_session->m_sessions.empty()){
            crow::response res;
            for(auto& session: m_session->m_sessions){
                if( session.isWebDriver && !session.isEnded){
                    auto driver = DriversFactory().create<Driver>(session.webdriverProcessName);
                    if(driver != nullptr){
                        driver->isResponseExecuted=true;
                        driver->deleteDriver(res, session.webdriverPort);
                        session.isEnded = true;
                    }
                }
            }
        }
    }

}