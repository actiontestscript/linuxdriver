#include <cmath>
#include "../../../include/W3C/capabilities/cap_os.hpp"
#include "../../../include/W3C/capabilities/capabilities.hpp"
#include "../../../include/W3C/session/session.hpp"

namespace ld {
    Capabilities::Type Os::type = Capabilities::Type::Os;

    std::unordered_map<std::string, std::string> parseOsRelease() {
        std::unordered_map<std::string, std::string> osInfo;
        std::ifstream file("/etc/os-release");

        if (!file.is_open()) {
            std::cerr << "Failed to open /etc/os-release" << std::endl;
            return osInfo;
        }

        std::string line;
        while (std::getline(file, line)) {
            size_t delimiter = line.find('=');
            if (delimiter != std::string::npos) {
                std::string key = line.substr(0, delimiter);
                std::string value = line.substr(delimiter + 1);

                // Remove quotes around the value if present
                if (!value.empty() && value.front() == '"' && value.back() == '"') {
                    value = value.substr(1, value.size() - 2);
                }

                osInfo[key] = value;
            }
        }

        file.close();
        return osInfo;
    }

    Os::Os() {
        auto osInfo = parseOsRelease();
        utsname uts{};

        if (uname(&uts) >= 0) {
            m_data.machineName = uts.nodename;
            m_data.version = osInfo["PRETTY_NAME"];
            m_data.processorArch = uts.machine;
            m_data.platformName = uts.sysname;
            m_data.platformVersion = uts.release;
        }

        m_data.driverVersion = std::to_string(LINUXDRIVER_VERSION_MAJOR) + "." + std::to_string(LINUXDRIVER_VERSION_MINOR) + "." + std::to_string(LINUXDRIVER_VERSION_PATCH) ;
        m_data.script = 30000;
        m_data.pageLoad = 300000;
        m_data.implicit = 0.0f;
        m_data.currentUrl = -1;
        m_data.buildNumber = m_data.platformVersion;
        setProcessorInfo();
        setDiskInfo();
        setScreenInfo();
    }

    bool Os::executeCapabilities(const crow::request& req, crow::response& res){
        CrowJsonResponse jRes;
        CrowJsonResponse::MsgError msgError;
        if(!_setOsBase(msgError)){
            jRes.setMsgErrorCode(msgError.error,"",msgError.stacktrace);
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return false;
        }

        return true;
    }

    void Os::printCapabilities(json &j){
        json capabilities;
        capabilities["browserName"] = m_data.platformName;
        capabilities["platformName"] = m_data.platformName;
        capabilities["platformVersion"] = m_data.version;
        capabilities["architecture"] = m_data.processorArch;
        capabilities["machine"] = m_data.machineName;
        json timeouts;
        timeouts["implicit"] = m_data.implicit;
        timeouts["pageLoad"] = m_data.pageLoad;
        if(m_data.script == 0) timeouts["script"] = nullptr;
        else timeouts["script"] = m_data.script;
        capabilities["timeouts"] = timeouts;
        j["capabilities"] = capabilities;
   }

    bool Os::getTimeouts(const std::string& sessionId,json& j){
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<Os*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        json timeouts;
        timeouts["implicit"] = caps->m_data.implicit;
        timeouts["pageLoad"] = caps->m_data.pageLoad;
        if(caps->m_data.script == 0) timeouts["script"] = nullptr;
        else timeouts["script"] = caps->m_data.script;
        j["value"] = timeouts;
        return true;
    }
    bool Os::setTimeouts(const std::string& sessionId,const crow::request& req) {
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<Os*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        const std::string& body = req.body;
        const json& src=json::parse(body);
        if(src.find("implicit") != src.end() && src["implicit"].is_number()){
            caps->m_data.implicit = src["implicit"];
        }
        if(src.find("pageLoad") != src.end() && src["pageLoad"].is_number()){
            caps->m_data.pageLoad = src["pageLoad"];
        }
        if(src.find("script") != src.end() && (src["script"].is_number() || src["script"].is_null() )){
            if (src["script"].is_null()) { caps->m_data.script = 0; }
            else { caps->m_data.script = src["script"]; }
        }
        return true;
    }


    bool Os::_setOsBase(CrowJsonResponse::MsgError& msgError) {
        msgError.clear();
        utsname uts{};
        if (uname(&uts) >= 0) {
            m_data.machineName = uts.nodename;
            m_data.version = uts.version;
            m_data.processorArch = uts.machine;
            m_data.platformName = uts.sysname;
            m_data.platformVersion = uts.release;
            return true;
        }
        else{
            msgError.error="unknown_error";
            msgError.stacktrace="Os::_setOsBase Error in uname";
            return false;
        }
    }

    bool Os::navigateTo(const std::string &sessionId, const crow::request &req) {
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<Os*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        std::string body = req.body;
        json src=json::parse(body);
        if(src.find("url") != src.end() && src["url"].is_string()){
            std::string url = src["url"];
            std::regex urlRegex("^(http|https|ftp)://.*");
            if(std::regex_match(url, urlRegex)){
                caps->m_data.urls.push_back(url);
                caps->m_data.currentUrl = static_cast<int>(caps->m_data.urls.size() -1 ) ;
                return true;
            }
        }

        return false;
    }

    bool Os::getCurrentUrl(const std::string &sessionId, json& j) {
        CrowJsonResponse jRes;
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<Os*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        json urls;
        if(caps->m_data.currentUrl == -1 ||caps->m_data.urls[caps->m_data.currentUrl].empty() ) urls["value"] = nullptr;
        else urls["value"] = caps->m_data.urls[caps->m_data.currentUrl];
        j = urls;
        return true;
    }

    bool Os::navigateBack(const std::string &sessionId) {
        CrowJsonResponse jRes;
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<Os*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        if(caps->m_data.currentUrl > 0){
            caps->m_data.currentUrl--;
            return true;
        }
        return true;
    }

    bool Os::navigateForward(const std::string &sessionId) {
        CrowJsonResponse jRes;
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<Os*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        if(caps->m_data.currentUrl < static_cast<int>(caps->m_data.urls.size() -1 ) ){
            caps->m_data.currentUrl++;
            return true;
        }
        return true;
    }

    void Os::setProcessorInfo() {
        std::ifstream cpuInfo("/proc/cpuinfo");
        std::string line;
        std::string key;
        std::string value;
        std::map<std::string, std::string> cpuInfoDetails{};
        std::regex pattern("[\\t\\n]");
        std::regex pattern1("^[\\s]+");
        std::vector<std::map<std::string, std::string>> cpuInfos{};
        while (std::getline(cpuInfo, line)) {
            line = std::regex_replace(line, pattern, "");
            std::istringstream iss(line);
            if (std::getline(iss, key, ':') && std::getline(iss, value)) {
                value = std::regex_replace(value, pattern1, "");
                if (key == "processor") {
                    if (!cpuInfoDetails.empty()) {
                        cpuInfos.push_back(cpuInfoDetails);
                        cpuInfoDetails.clear();
                    }
                    cpuInfoDetails[key] = value;
                } else
                    cpuInfoDetails[key] = value;
            }
        }
        cpuInfos.push_back(cpuInfoDetails);
        m_data.processorCoreNumber = std::to_string(cpuInfos.size());

        m_data.processorSocket = cpuInfoDetails["model name"];

        //model name
        m_data.processorName  = "Family " + cpuInfoDetails["cpu family"];
        m_data.processorName += " Model " + cpuInfoDetails["model"];
        m_data.processorName += " Stepping " + cpuInfoDetails["stepping"];

        //cpu Mhz
        int cpuSpeed = 0;
        if(Utils::isFloat(cpuInfoDetails["cpu MHz"])) {
            cpuSpeed = (static_cast<int>(std::ceil(std::stof(cpuInfoDetails["cpu MHz"]))));
            m_data.processorSpeedGhz = static_cast<long>(std::ceil(std::stof(cpuInfoDetails["cpu MHz"])) / 1000.0f);
        }
        else if(Utils::isInt(cpuInfoDetails["cpu MHz"])) {
            cpuSpeed = std::stoi(cpuInfoDetails["cpu MHz"]);
            m_data.processorSpeedGhz = static_cast<long>(std::ceil(std::stoi(cpuInfoDetails["cpu MHz"])) / 1000);
        }
        if(cpuSpeed > 0)
            m_data.processorSpeed = std::to_string(cpuSpeed) + " MHz";
        else
            m_data.processorSpeed = cpuInfoDetails["cpu MHz"]+ " MHz";

    }

    void Os::setDiskInfo() {
        if(!Session::getInstance()->getIsDockerContainer()) {
            char path[PATH_MAX];
            ssize_t count = readlink("/proc/self/exe", path, PATH_MAX);
            if (count == -1) { return; }
            const std::string exePath(path, count);

            struct stat fsInfo{};
            if (stat(exePath.c_str(), &fsInfo) == -1) { return; }
            // @TODO check if exist on suze ; fedora ;
            // @TODO check if execPath mntPoint is the same as /proc/mounts

            FILE *mountTable = setmntent("/proc/mounts", "r");
            if (mountTable == nullptr) { return; }
            struct mntent *mountEntry;
            while ((mountEntry = getmntent(mountTable)) != nullptr) {
                if (mountEntry->mnt_fsname[0] != '/') {
                    continue;
                }
                struct stat mountFsInfo{};
                if (stat(mountEntry->mnt_dir, &mountFsInfo) == -1) {
                    continue;
                }
                if (fsInfo.st_dev == mountFsInfo.st_dev) {
                    //m_data.systemDriveMountPoint = mountEntry->mnt_dir;
                    m_data.systemDriveMountPoint = mountEntry->mnt_fsname;
                    break;
                }
            }

            endmntent(mountTable);

            //disk Space
            std::string result{};
            std::string cmdFullSize = "df -BM " + m_data.systemDriveMountPoint + " | tail -1 | awk '{print $2}'";
            std::string cmdFreeSize = "df -BM " + m_data.systemDriveMountPoint + " | tail -1 | awk '{print $4}'";
//            LD_LOG_DEBUG << "cmdFullSize : " << cmdFullSize << std::endl;
//            LD_LOG_DEBUG << "cmdFreeSize : " << cmdFreeSize << std::endl;
            std::regex pattern("[\\t\\n]");
            if (Utils::exec(cmdFullSize.c_str(), result)) {
                result = std::regex_replace(result, pattern, "");
                m_data.systemDriveTotalSize = result + " Mo";
            }

            result = "";
            if (Utils::exec(cmdFreeSize.c_str(), result)) {
                result = std::regex_replace(result, pattern, "");
                m_data.systemDriveFreeSpace = result + " Mo";
            }
        }
        else {
            m_data.systemDriveMountPoint = "/docker containers/";
            m_data.systemDriveFreeSpace = "1000 Mo";
            m_data.systemDriveTotalSize = "2000 Mo";
            LD_LOG_DEBUG << "systemDriveMountPoint : " << m_data.systemDriveMountPoint << std::endl;
            LD_LOG_DEBUG << "systemDriveFreeSpace : " << m_data.systemDriveFreeSpace << std::endl;
            LD_LOG_DEBUG << "systemDriveTotalSize : " << m_data.systemDriveTotalSize << std::endl;


        }

    }

    void Os::setScreenInfo() {

        bool isGraphic = Session::getInstance()->getEnvironment().getGraphicalEnvironment();
        if (isGraphic) {
            const char *xdg_session_type = std::getenv("XDG_SESSION_TYPE");
            if (xdg_session_type == nullptr) { return; }
            else if (strcmp(xdg_session_type, "x11") != 0) {
                m_data.graphicsInterface = OsData::GraphicsInterface::X11;
                m_data.withGraphicsInterface = false;
            } //interface X11
            else if (strcmp(xdg_session_type, "wayland") != 0) {
                m_data.graphicsInterface = OsData::GraphicsInterface::Wayland;
                m_data.withGraphicsInterface = false;
            } //interface wayland
            else if (strcmp(xdg_session_type, "tty") != 0) {
                m_data.graphicsInterface = OsData::GraphicsInterface::None;
                m_data.headlessMode = true;
                m_data.withGraphicsInterface = true;
            } //ss interface

            //xrandr
            std::string result{};
            std::string cmd = "which xrandr";

            if (Utils::exec(cmd.c_str(), result)) {
                if (result.empty()) return;
            }
            result = "";
            cmd = "xrandr | grep '*' | awk '{print $1}'";
            std::regex pattern("[\\t\\n]");
            if (Utils::exec(cmd.c_str(), result)) {
                result = std::regex_replace(result, pattern, "");
                //split
                size_t p = result.find('x');
                m_data.screenWidth = m_data.virtualWidth = result.substr(0, p);
                m_data.screenHeight = m_data.virtualHeight = result.substr(p + 1);
            }
            m_data.interactive = true;
        }
        else{
            m_data.graphicsInterface = OsData::GraphicsInterface::None;
            m_data.headlessMode = true;
            m_data.withGraphicsInterface = true;
            m_data.screenWidth = "1";
            m_data.screenHeight = "1";
            m_data.interactive = false;
        }
    }


}// namespace ld
