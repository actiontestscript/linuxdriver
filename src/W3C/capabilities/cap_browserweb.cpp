//
// Created by eric on 23/02/23.
//
#include "../../../include/W3C/capabilities/cap_browserweb.hpp"
namespace ld {
    Capabilities::Type BrowserWeb::type = Capabilities::Type::BrowserWeb;

    bool BrowserWeb::executeCapabilities(const crow::request& req, crow::response& res){
         json jsonBody = json::parse(req.body);

         CrowJsonResponse jRes;
         CrowJsonResponse::MsgError msgError;

         if(!mandatoryFields(const_cast<json &>(jsonBody))){
             jRes.setMsgErrorCode("invalid_argument","","message: The mandatory fields are not present in the json");
             res.code = jRes.m_errorCode;
             res.write(jRes.jsonMsgError().dump());
             return false;
         }

         if(!checkCapabilities(jsonBody, msgError)){
             jRes.setMsgErrorCode(msgError.error, msgError.message, msgError.stacktrace);
             res.code = jRes.m_errorCode;
             res.write(jRes.jsonMsgError().dump());
             return false;
         }

         mergeCapabilities(jsonBody);
         selectElement();

         return true;
     }

    bool BrowserWeb::mandatoryFields(json &j) {
        if(j.find("capabilities") != j.end()){
            json capabilities = j["capabilities"];
            if(capabilities.find("firstMatch") != capabilities.end()){
                json firstMatch = capabilities["firstMatch"];
                if(firstMatch.is_array() ) {
                    for (auto &item: firstMatch) {
                        if (item.find("browserName") != item.end()) {
                            if (!item["browserName"].empty()) {
                                return true;
                            }
                        }
                    }
                }
                if(firstMatch.is_object() ) {
                    if (firstMatch.find("browserName") != firstMatch.end()) {
                        if (!firstMatch["browserName"].empty()) {
                            LD_LOG_DEBUG << "browserName: " << firstMatch["browserName"] << std::endl;
                            return true;
                        }
                    }
                }
            }

            if(capabilities.find("alwaysMatch") != capabilities.end()) {
                json alwaysMatch = capabilities["alwaysMatch"];
                if(alwaysMatch.is_array() ) {
                    for(auto& item : alwaysMatch){
                        LD_LOG_DEBUG << "item always : " << item<< std::endl;
                        if(item.find("browserName") != item.end()){
                            if(!item["browserName"].empty() ){
                                return true;
                            }
                        }
                    }
                }
                if(alwaysMatch.is_object() ) {
                    if (alwaysMatch.find("browserName") != alwaysMatch.end()) {
                        if (!alwaysMatch["browserName"].empty()) {
                            LD_LOG_DEBUG << "browserName: " << alwaysMatch["browserName"] << std::endl;
                            return true;
                        }
                    }
                }

            }
        }
        return false;
    }

    bool BrowserWeb::checkCapabilities(json &j, CrowJsonResponse::MsgError& msgError) {
        msgError.clear();
        if(j.find("capabilities") != j.end()) {
            json capabilities = j["capabilities"];
            if (capabilities.find("firstMatch") != capabilities.end()) {
                json firstMatch = capabilities["firstMatch"];
                if (firstMatch.is_array()) {
                    for (auto &item: firstMatch) {
                        if (!_checkIsValidElements(item,msgError)) return false;
                    }
                }
                else if (firstMatch.is_object()) {
                    if (!_checkIsValidElements(firstMatch,msgError)) return false;
                }

                if (capabilities.find("alwaysMatch") != capabilities.end()) {
                    json alwaysMatch = capabilities["alwaysMatch"];
                    if (alwaysMatch.is_array()) {
                        for (auto &item: alwaysMatch) {
                            if (!_checkIsValidElements(item,msgError)) return false;
                        }
                    } else if (alwaysMatch.is_object()) {
                        if (!_checkIsValidElements(alwaysMatch,msgError)) return false;
                    }
                }
            }
        }
        return true;
    }

    bool BrowserWeb::_checkIsValidElements(json &j,CrowJsonResponse::MsgError& msgError) {
        auto value = j.dump();
        if (j.find("acceptInsecureCerts") != j.end()) {
            if (!j["acceptInsecureCerts"].is_boolean()) {
                msgError.stacktrace = "acceptInsecureCerts is not a correct boolean value";
                msgError.error = "invalid_argument";
                return false;
            }
        }
        if (j.find("browserName") != j.end()){
            if(!j["browserName"].is_string()){
                msgError.stacktrace = "message: browserName is not a correct string value";
                msgError.error = "invalid_argument";
                return false;
            }
        }
        if (j.find("browserVersion") != j.end()){
            if(!j["browserVersion"].is_string()){
                msgError.stacktrace = "message: browserVersion is not a correct string value";
                msgError.error = "invalid_argument";
                return false;
            }
        }
        if (j.find("platformName") != j.end()){
            if(!j["platformName"].is_string()){
                msgError.stacktrace = "message: platformName is not a correct string value";
                msgError.error = "invalid_argument";
                return false;
            }
        }
        if (j.find("strictFileInteractability") != j.end()){
            if(!j["strictFileInteractability"].is_boolean()){
                msgError.stacktrace = "message: strictFileInteractability is not a correct boolean value";
                msgError.error = "invalid_argument";
                return false;
            }
        }
        if(j.find("pageLoadStrategy") != j.end()){
            if(!j["pageLoadStrategy"].is_string()){
                msgError.stacktrace = "message: pageLoadStrategy is not a correct string value";
                msgError.error = "invalid_argument";
                return false;
            }
            else{
                std::string pageLoadStrategy = j["pageLoadStrategy"];
                if(pageLoadStrategy != "normal" && pageLoadStrategy != "eager" && pageLoadStrategy != "none"){
                    msgError.stacktrace = "message: pageLoadStrategy is incorrectly defined value : normal, eager or none";
                    msgError.error = "invalid_argument";
                    return false;
                }
            }
        }
        if(j.find("proxy") != j.end()){
            json proxy = j["proxy"];
            if(!proxy["proxyType"].is_string()){
                msgError.stacktrace = "message: proxyType is not a correct string value";
                msgError.error = "invalid_argument";
                return false;
            }
            else{
                std::string proxyType = proxy["proxyType"];
                if(proxyType != "direct" && proxyType != "pac" && proxyType != "autodetect" && proxyType != "system" && proxyType != "manual"){
                    msgError.stacktrace = "message: proxyType is incorrectly defined value : direct, pac, autodetect, system or manual";
                    msgError.error = "invalid_argument";
                    return false;
                }
            }
        }
        /*
        else if (j.find("unhandledPromptBehavior") != j.end()){
            if(!j["unhandledPromptBehavior"].is_string()){
                return false;
            }
        }

        else if (j.find("setWindowRect") != j.end()){
            if(!j["setWindowRect"].is_boolean()){
                return false;
            }
        }*/
        return true;
    }

    void BrowserWeb::printCapabilities(json &j){
        m_data = m_dataVector[getDataSelected()];
        json capabilities;
        capabilities["browserName"] = m_data.browserName;
        if(! m_data.browserVersion.empty() )capabilities["browserVersion"] = m_data.browserVersion;
        if(! m_data.platformName.empty() ) capabilities["platformName"] = m_data.platformName;
        capabilities["acceptInsecureCerts"] = m_data.acceptInsecureCerts;
        capabilities["pageLoadStrategy"] = m_data.pageLoadStrategy;
        capabilities["setWindowRect"] = m_data.setWindowRect;
        if(! m_data.unhandledPromptBehavior.empty() ) capabilities["unhandledPromptBehavior"] = m_data.unhandledPromptBehavior;
        capabilities["strictFileInteractability"] = m_data.strictFileInteractability;
        json proxy;
        bool proxySet = false;
        if(! m_data.proxyType.empty() ) {proxy["proxyType"] = m_data.proxyType; proxySet = true;}
        if(! m_data.proxyAutoconfigUrl.empty() ){ proxy["proxyAutoconfigUrl"] = m_data.proxyAutoconfigUrl; proxySet = true;}
        if(! m_data.ftpProxy.empty() ){ proxy["ftpProxy"] = m_data.ftpProxy;proxySet = true;}
        if(! m_data.httpProxy.empty() ){ proxy["httpProxy"] = m_data.httpProxy;proxySet = true;}
        if(! m_data.sslProxy.empty() ){ proxy["sslProxy"] = m_data.sslProxy;proxySet = true;}
        if(! m_data.socksProxy.empty() ){ proxy["socksProxy"] = m_data.socksProxy;proxySet = true;}
        if(m_data.socksVersion ){ proxy["socksVersion"] = m_data.socksVersion;proxySet = true;}
        if(! m_data.noProxy.empty() ){ proxy["noProxy"] = m_data.noProxy;proxySet = true;}
        if(proxySet) capabilities["proxy"]   = proxy;
        else capabilities["proxy"] = json::object();
        json timeouts;
        timeouts["implicit"] = m_data.implicit;
        timeouts["pageLoad"] = m_data.pageLoad;
        timeouts["script"] = m_data.script;
        capabilities["timeouts"] = timeouts;
        j["capabilities"] = capabilities;
    }

    void BrowserWeb::mergeCapabilities(json &j){
        // find in firstmatch if exist one or more  browsername
        if(j.find("capabilities") != j.end()) {
            json capabilities = j["capabilities"];
            if (capabilities.find("firstMatch") != capabilities.end()) {
                json firstMatch = capabilities["firstMatch"];
                if (firstMatch.is_array()) {
                    for (auto &item: firstMatch) {
                        if (item.find("browserName") != item.end()) {
                            BrowserWebData data{};
                            data.isFirstMatchBrowserName = true;
                            setDataValue(item, data);
                            m_dataVector.push_back(data);
                        }
                    }
                } else if (firstMatch.is_object()) {
                    if (firstMatch.find("browserName") != firstMatch.end()) {
                        BrowserWebData data{};
                        data.isFirstMatchBrowserName = true;
                        setDataValue(firstMatch, data);
                        m_dataVector.push_back(data);
                    }
                }
            }}

        // 2 if yes, find if it also exists in alwaysmatch
        if(!m_dataVector.empty()){
            if(j.find("capabilities") != j.end()) {
                json capabilities = j["capabilities"];
                if (capabilities.find("alwaysMatch") != capabilities.end()) {
                    json alwaysMatch = capabilities["alwaysMatch"];
                    if (alwaysMatch.is_object()) {
                        if (alwaysMatch.find("browserName") != alwaysMatch.end()) {
                            for(auto &data : m_dataVector){
                                if (data.browserName == alwaysMatch["browserName"]){
                                    data.isAlwaysMatchBrowserName = true;
                                    setDataValue(alwaysMatch, data);
                                }
                            }
                        }
                        else{
                            //always commun a  tous
                            for(auto &data : m_dataVector){
                                setDataValue(alwaysMatch, data);
                            }
                        }
                    }
                    else if(alwaysMatch.is_array()){
                        for(auto &item : alwaysMatch){
                            if (item.find("browserName") != item.end()) {
                                for(auto &itemData : m_dataVector){
                                    if (itemData.browserName == item["browserName"]){
                                        setDataValue(item, itemData);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        // 3 - no browserame found in firstmatch / scan alwaysmatch
        else{
            if(j.find("capabilities") != j.end()) {
                json capabilities = j["capabilities"];
                if (capabilities.find("alwaysMatch") != capabilities.end()) {
                    json alwaysMatch = capabilities["alwaysMatch"];
                    if (alwaysMatch.is_object()) {
                        BrowserWebData data{};
                        data.isAlwaysMatchBrowserName = true;
                        setDataValue(alwaysMatch, data);
                        m_dataVector.push_back(data);
                    }
                    else if(alwaysMatch.is_array()){
                        for(auto &item : alwaysMatch){
                            if (item.find("browserName") != item.end()) {
                                BrowserWebData data{};
                                data.isAlwaysMatchBrowserName = true;
                                setDataValue(item, data);
                                m_dataVector.push_back(data);
                            }
                        }
                    }
                }
            }
            else {return;}  // no browsername found in firstmatch and alwaysmatch no possible to continue  //dataVector empty

            //merge firstmatch element to alwaysmatch browsername
            for(auto &data : m_dataVector){
                if(j.find("capabilities") != j.end()) {
                    json capabilities = j["capabilities"];
                    if (capabilities.find("firstMatch") != capabilities.end()) {
                        json firstMatch = capabilities["firstMatch"];
                        if (firstMatch.is_array()) {
                            for (auto &item: firstMatch) {
                                setDataValue(item, data);
                            }
                        } else if (firstMatch.is_object()) {
                            setDataValue(firstMatch, data);
                        }
                    }
                }
            }
        }
    }

    void BrowserWeb::selectElement(){
        if(m_dataVector.empty()){
            m_data = BrowserWebData{};
            m_dataVector.push_back(m_data);
            setDataSelected(0);
            return;
        }
        if(m_dataVector.size() == 1){
            m_data = m_dataVector[0];
            setDataSelected(0);
            return;
        }
        for(int i = static_cast<int>(m_dataVector.size()) - 1 ; i >= 0 ; i--){
            if(m_dataVector[i].isAlwaysMatchBrowserName && m_dataVector[i].isFirstMatchBrowserName){
                this->m_data = m_dataVector[i];
                setDataSelected(i);
                return;
            }
        }

        for(int i = static_cast<int>(m_dataVector.size()) - 1 ; i >= 0 ; i--) {
            if (m_dataVector[i].isFirstMatchBrowserName) {
                this->m_data = m_dataVector[i];
                setDataSelected(i);
                return;
            }
        }
        for(int i = static_cast<int>(m_dataVector.size()) - 1 ; i >= 0 ; i--) {
            if (m_dataVector[i].isAlwaysMatchBrowserName) {
                this->m_data = m_dataVector[i];
                setDataSelected(i);
                return;
            }
        }
     }

    void BrowserWeb::setDataValue(const json &j, BrowserWebData& data){
        if(j.find("browserName") != j.end() && data.browserName.empty()) data.browserName = j["browserName"];
        if(j.find("browserVersion") != j.end() && data.browserVersion.empty()) data.browserVersion = j["browserVersion"];
        if(j.find("platformName") != j.end() && data.platformName.empty()) data.platformName = j["platformName"];
        if(j.find("acceptInsecureCerts") != j.end() && !data.isSetAcceptInsecureCerts ){data.acceptInsecureCerts = j["acceptInsecureCerts"];data.isSetAcceptInsecureCerts=true;}
        if(j.find("pageLoadStrategy") != j.end() && !data.isSetPageLoadStrategy ) {data.pageLoadStrategy = j["pageLoadStrategy"];data.isSetPageLoadStrategy=true;}
        if(j.find("setWindowRect") != j.end() && !data.isSetSetWindowRect ){data.setWindowRect = j["setWindowRect"];data.isSetSetWindowRect=true;}
        if(j.find("unhandledPromptBehavior") != j.end() && data.unhandledPromptBehavior.empty()) data.unhandledPromptBehavior = j["unhandledPromptBehavior"];
        if(j.find("strictFileInteractability") != j.end() && !data.isSetStrictFileInteractability ){data.strictFileInteractability = j["strictFileInteractability"];data.isSetStrictFileInteractability=true;}
        if(j.find("proxy") != j.end() ){
            json proxy = j["proxy"];
            if(proxy.find("proxyType") != proxy.end() && data.proxyType.empty()) data.proxyType = proxy["proxyType"];
            if(proxy.find("proxyAutoconfigUrl") != proxy.end() && data.proxyAutoconfigUrl.empty()) data.proxyAutoconfigUrl = proxy["proxyAutoconfigUrl"];
            if(proxy.find("ftpProxy") != proxy.end() && data.ftpProxy.empty()) data.ftpProxy = proxy["ftpProxy"];
            if(proxy.find("httpProxy") != proxy.end() && data.httpProxy.empty()) data.httpProxy = proxy["httpProxy"];
            if(proxy.find("sslProxy") != proxy.end() && data.sslProxy.empty()) data.sslProxy = proxy["sslProxy"];
            if(proxy.find("socksProxy") != proxy.end() && data.socksProxy.empty()) data.socksProxy = proxy["socksProxy"];
            if(proxy.find("socksVersion") != proxy.end() && !data.isSetSocksVersion ){data.socksVersion = proxy["socksVersion"];data.isSetSocksVersion=true;}
            if(proxy.find("noProxy") != proxy.end() && !data.isNoProxy) data.noProxy = proxy["noProxy"];
        }
        if(j.find("timeouts") != j.end() ){
            json timeouts = j["timeouts"];
            if(timeouts.find("implicit") != timeouts.end() && data.implicit == 0.0f )data.implicit = timeouts["implicit"];
            if(timeouts.find("pageLoad") != timeouts.end() && data.pageLoad == 300000 )data.pageLoad = timeouts["pageLoad"];
            if(timeouts.find("script") != timeouts.end() && data.script == 30000)data.script = timeouts["script"];
        }
    }

    bool BrowserWeb::getTimeouts(const std::string& sessionId,json& j){
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<BrowserWeb*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        json timeouts;
        timeouts["implicit"] = caps->m_data.implicit;
        timeouts["pageLoad"] = caps->m_data.pageLoad;
        if(caps->m_data.script == 0) timeouts["script"] = nullptr;
        else timeouts["script"] = caps->m_data.script;
        j["value"] = timeouts;
        return true;
     }

    bool BrowserWeb::setTimeouts(const std::string& sessionId,const crow::request& req) {
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<BrowserWeb*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        std::string body = req.body;
        json src=json::parse(body);
        if(src.find("implicit") != src.end() && src["implicit"].is_number()){
            caps->m_data.implicit = src["implicit"];
        }
        if(src.find("pageLoad") != src.end() && src["pageLoad"].is_number()){
            caps->m_data.pageLoad = src["pageLoad"];
        }
        if(src.find("script") != src.end() && (src["script"].is_number() || src["script"].is_null() )){
            if (src["script"].is_null()) { caps->m_data.script = 0; }
            else { caps->m_data.script = src["script"]; }
        }
        return true;
    }

    bool BrowserWeb::navigateTo(const std::string& sessionId,const crow::request& req){
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<BrowserWeb*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        std::string body = req.body;
        json src=json::parse(body);
        if(src.find("url") != src.end() && src["url"].is_string()){
            std::string url = src["url"];
            std::regex urlRegex("^(http|https|ftp)://.*");
            if(std::regex_match(url, urlRegex)){
                caps->m_data.urls.push_back(url);
                caps->m_data.currentUrl = static_cast<int>(caps->m_data.urls.size() -1 ) ;
                return true;
            }
        }
        return false;

    }

    bool BrowserWeb::getCurrentUrl(const std::string &sessionId, json& j) {
        CrowJsonResponse jRes;
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<BrowserWeb*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        json urls;
        if(caps->m_data.currentUrl == -1 || caps->m_data.urls[caps->m_data.currentUrl].empty() ) urls["value"] = nullptr;
        else urls["value"] = caps->m_data.urls[caps->m_data.currentUrl];
        j = urls;
        return true;
    }

    bool BrowserWeb::navigateBack(const std::string &sessionId) {
        CrowJsonResponse jRes;
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<BrowserWeb*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        if(caps->m_data.currentUrl > 0){
            caps->m_data.currentUrl--;
            return true;
        }
        return true;
    }

    bool BrowserWeb::navigateForward(const std::string &sessionId) {
        CrowJsonResponse jRes;
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<BrowserWeb*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        if(caps->m_data.currentUrl < static_cast<int>(caps->m_data.urls.size() -1 ) ){
            caps->m_data.currentUrl++;
            return true;
        }
        return true;
    }
}//end namespace ld
