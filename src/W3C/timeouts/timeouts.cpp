#include "../../../include/W3C/timeouts/timeouts.hpp"
namespace ld {
    Timeouts::Timeouts() {
        m_session = Session::getInstance();
    }

    void Timeouts::getTimeouts(const std::string &sessionId, crow::response &res) {
        getJsonHeaderResponse(res);
        CrowJsonResponse jRes;
        SessionData::SessionInfo* sessionInfo = m_session->getSessionInfo(sessionId);
        bool noSession = (sessionInfo == nullptr || m_session->isEndedSession(sessionId));
        if(noSession){
            jRes.setMsgErrorCode("invalid_session_id","","");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return;
        }
        json jsonMsgReturn;
        auto caps = CapabilitiesFactory().create<Capabilities>(sessionInfo->nameCapabilities);
        if(!caps->getTimeouts(sessionId,jsonMsgReturn)){
            jRes.setMsgErrorCode("invalid_session_id","","");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return;
        }

        res.code = 200;
        res.write(jsonMsgReturn.dump());
    }

    void Timeouts::setTimeouts(const std::string& sessionId,const crow::request &req, crow::response& res){
        getJsonHeaderResponse(res);
        CrowJsonResponse jRes;
        SessionData::SessionInfo* sessionInfo = m_session->getSessionInfo(sessionId);
        bool noSession = (sessionInfo == nullptr || m_session->isEndedSession(sessionId));
        if(noSession){
            jRes.setMsgErrorCode("invalid_session_id","","");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return;
        }
        //Common Verifications for all Json
        std::string body = req.body;
        if(!jRes.isValidJson(body)){
            jRes.setMsgErrorCode("invalid_argument","","Is not a valid JSON string");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return;
        }
        json jsonMsgReturn;
        auto caps = CapabilitiesFactory().create<Capabilities>(sessionInfo->nameCapabilities);
        if(!caps->setTimeouts( sessionId, req)){
            jRes.setMsgErrorCode("invalid_session_id","","");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return;
        }
        jsonMsgReturn["value"] = nullptr;
        res.code = 200;
        res.write(jsonMsgReturn.dump());

    }

}
