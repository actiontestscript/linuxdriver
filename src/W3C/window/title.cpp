#include "../../../include/W3C/window/title.hpp"
namespace ld{
    bool WindowTitle::_jsonIsValid(const crow::request &req, crow::response &res){
        const std::string& body = req.body;
        if(!isValidJson(body)){
            LD_LOG_ERROR << jsonMsgError().dump() << std::endl;
            res.code = m_errorCode;
            res.write(jsonMsgError().dump());
            return false;
        }
        return true;
    }

    bool WindowTitle::execute(const crow::request &req, crow::response &res) {
        //check if json is valid
        if (!_jsonIsValid(req, res)) return false;
        //parseJson
        _parseJson(req);


        //parse postData
        //_parsePostData(req);
        if(m_driverName.empty()){
            // todo response driverName null
            getJsonNullResponse(res);
            return false;
        }
        auto driver = DriversFactory().create<Driver>(m_driverName);
        if (driver == nullptr) {
            // todo response driverName incorrect
            getJsonNullResponse(res);
            return false;
        }

        m_session = m_session->getInstance();
        Session::ProcessInfo processInfo = m_session->getProcessInfo(m_driverName);

        m_windowTitleData.pid = -1;
        if(processInfo.port != "0"){
            std::string sPid = Session::getProcessPid(processInfo);
            std::vector<std::string> browserPids{};
            if(!sPid.empty()) {
                browserPids = Session::getBrowserPid(sPid);
                if (!browserPids.empty()) m_windowTitleData.pid = std::stoi(browserPids[0]);
            }
        }
//        m_windowTitleData.pid = Session::getProcessPid();
        m_windowTitleData.appIcon = driver->getIconBrowserBase64();
        _setWindowTitleData();

        printCapabilities(res);

        return true;
    }

    bool WindowTitle::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if (jsonBody.find("value") != jsonBody.end()) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "title", m_titleUid);
            CrowJsonResponse::jsonParse(value, "name", m_driverName);
            return true;
        }
        return false;
    }

    bool WindowTitle::_parsePostData(const crow::request &req) {
        std::string data = req.body;

        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_titleUid);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_driverName);
        return true;
    }

    void WindowTitle::_strParseString(std::string &strResearch, const char &delimiter, const std::string &defaultValue, std::string &data) {
        size_t pos{};
        pos = strResearch.find(delimiter);
        std::string dataStr = strResearch.substr(0, pos);
        if(!dataStr.empty()) data = dataStr;
        else data = defaultValue;
        strResearch.erase(0, pos + 1);
    }

    void WindowTitle::printCapabilities(crow::response &res) {
        getJsonHeaderResponse(res);
        json windowTitleInfo;
        windowTitleInfo["id"] = m_windowTitleData.id;
        windowTitleInfo["pid"] = m_windowTitleData.pid;
        windowTitleInfo["handle"] = m_windowTitleData.handle;
        windowTitleInfo["tag"] = m_windowTitleData.tag;
        windowTitleInfo["width"] = m_windowTitleData.width;
        windowTitleInfo["height"] = m_windowTitleData.height;
        windowTitleInfo["x"] = m_windowTitleData.x;
        windowTitleInfo["y"] = m_windowTitleData.y;
        json appInfo;
        appInfo["version"] = m_windowTitleData.appBuildVersion;
        appInfo["build"] = m_windowTitleData.appVersion;
        appInfo["name"] = m_windowTitleData.appName;
        appInfo["path"] = m_windowTitleData.appPath;
        appInfo["icon"] = m_windowTitleData.appIcon;
        windowTitleInfo["app"] = appInfo;

        json value;
        value["value"] = windowTitleInfo;
        res.write(value.dump());
        res.code = 200;
    }

    void WindowTitle::_setWindowTitleData() {
        m_session = m_session->getInstance();

        //info driverName load
        {
            std::lock_guard<std::mutex> lock(m_session->m_mutex);
            for (auto &session: m_session->m_sessions) {
                if (session.webdriverName == m_driverName && !session.isEnded) {
//                    m_windowTitleData.id = Guid::generate(static_cast<int>(Guid::GuidVersion::V4));
                    m_windowTitleData.id = m_titleUid;
                    m_windowTitleData.appName = session.applicationName;
                    m_windowTitleData.appPath = session.applicationPath;
                    m_windowTitleData.appVersion = session.applicationVersion;
                    m_windowTitleData.appBuildVersion = session.applicationVersion;
                }
            }
        }

    }

}// namespace ld

