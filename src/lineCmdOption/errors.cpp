#include "../../include/lineCmdOption/errors.hpp"

namespace ld {
    Errors::Errors() {
        ErrorsData::ErrorInfo error;
        error.errorNumber = 1;
        error.errorMessage = "Error 1 - invalid options\n";
        error.gravity = CRITICAL;
        m_errors.push_back(error);

        /////////////// Errors Options //////////////////
        //options -p port
        error.errorNumber = 2;
        error.errorMessage = "Error 2 - wrong input format in options port format --port <value> between 1024 and 65535\n ";
        error.gravity = CRITICAL;
        m_errors.push_back(error);

        //options -ip address
        error.errorNumber = 3;
        error.errorMessage = "Error 3 - wrong input format in options ip format --bind-address <value>,<optional value ip address> \n ";
        error.gravity = CRITICAL;
        m_errors.push_back(error);

        //wrong hostname
        error.errorNumber = 4;
        error.errorMessage = "Error 4 - wrong hostname value \n ";
        error.gravity = CRITICAL;
        m_errors.push_back(error);

        //wrong hostname in local machine
        error.errorNumber = 5;
        error.errorMessage = "Error 5 - wrong hostname value - the hostname is not a local address \n ";
        error.gravity = CRITICAL;
        m_errors.push_back(error);

        //wrong AllWebDriver value in local machine
        error.errorNumber = 6;
        error.errorMessage = "Error 6 - wrong allWebDriver value - true or false value only \n ";
        error.gravity = CRITICAL;
        m_errors.push_back(error);

        //wrong loglevel value in local machine
        error.errorNumber = 7;
        error.errorMessage = "Error 7 - wrong loglevel value : authorized value : all, debug, info, error, warning, silent \n ";
        error.gravity = CRITICAL;
        m_errors.push_back(error);

        //wrong loglevel value in local machine
        error.errorNumber = 8;
        error.errorMessage = "Error 8 - wrong local value : --local \n ";
        error.gravity = CRITICAL;
        m_errors.push_back(error);

        //wrong GlobalIp value in local machine
        error.errorNumber = 9;
        error.errorMessage = "Error 9 - wrong global IP value : --globalip=xxx.xxx.xxx.xxx IPV4 address \n ";
        error.gravity = CRITICAL;
        m_errors.push_back(error);

        //port is not available
        error.errorNumber = 10;
        error.errorMessage = "Error 10 - The port is not available \n ";
        error.gravity = CRITICAL;
        m_errors.push_back(error);

        //wrong silent value
        error.errorNumber = 11;
        error.errorMessage = "Error 11 - wrong silent value : --silent \n ";
        error.gravity = CRITICAL;
        m_errors.push_back(error);

        //wrong help value
        error.errorNumber = 12;
        error.errorMessage = "Error 12 - wrong help value : --help \n ";
        error.gravity = CRITICAL;
        m_errors.push_back(error);
    };

    const ErrorsData::ErrorInfo Errors::getError(int errorNumber) {
        for (auto a: m_errors) {
            if (a.errorNumber == errorNumber) return a;
        }
        return ErrorsData::ErrorInfo();
    };


    void Errors::displayError(int errorNumber) {
        LD_LOG_ALWAYS << getError(errorNumber).errorMessage << endl;
    }
}