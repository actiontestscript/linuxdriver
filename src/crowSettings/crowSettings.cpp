//
// Created by eric on 07/02/23.
//
#include "../../include/crowSettings/crowSettings.hpp"
namespace ld {

    CrowSettings::CrowSettings(Errors* errors, OptionLineCmd* optionsData):m_errors(errors), m_optionsData(optionsData), m_port(9700) {
        _setPort();
        _setListIps();
    }


    bool CrowSettings::_isPortSetup(){
        //m_optionsData->setOptionsIsCmdSet("port", true);
        bool res = false;
        if (m_optionsData->getOptions().find("port") != m_optionsData->getOptions().end()) {
            res =  m_optionsData->getOptions().at("port").isCmdLineSet;
       }
        return res;
    }

    void CrowSettings::_setListIps() {
        m_ips.clear();
        /*
        if (m_optionsData->getOptions().find("bind-address") != m_optionsData->getOptions().end()) {
            string hostnameLists = m_optionsData->getOptions().at("bind-address").value;
            size_t p;
            do{
                p = hostnameLists.find(',');
                string hostname = hostnameLists.substr(0, p);
                hostname = _checkHostname(hostname);

                if (hostname.empty()) {
                    m_errors->displayError(m_optionsData->getNumError());
                    exit(EXIT_FAILURE);
                }
                if(!_isIpList(hostname)) m_ips.push_back(hostname);

                if(p != string::npos) {
                    hostnameLists.erase(0, p + 1);
                }
            }while (p != string::npos);
        }
         */
        bool isLocal = false;
        if(Utils::isBool(m_optionsData->getOptions().at("local").value)){
            isLocal = Utils::strToBool(m_optionsData->getOptions().at("local").value);
        };
        if(!isLocal) {
            m_ips.emplace_back("0.0.0.0");
        }
        else {
            m_ips.emplace_back("127.0.0.1");
        }
    }

    void CrowSettings::_setPort() {
        if (m_optionsData->getOptions().find("port") != m_optionsData->getOptions().end()) {
            m_port = stoi(m_optionsData->getOptions().at("port").value);
        }
        if (!_isPortSetup()) {
            m_port = Network::getAvailablePort(m_port, m_port, "localhost");
            if (m_port == -1) {
                LD_LOG_ERROR << "Port not available" << std::endl;
            }
        }
        else {
            m_port = Network::getAvailablePort(m_port, 65535, "localhost");
            if (m_port == -1 ) {
                LD_LOG_ERROR << "Port not available" << std::endl;
            }
        }
    }
/*
    int CrowSettings::_checkPort(const int &portStart,const int &PortEnd, const string &ip) {
        bool findPort = false;
        int i = portStart;
        int res = 0;
        while (res == 0 && i <= PortEnd) {
            struct sockaddr_in server{};
            memset(&server, 0, sizeof(server));
            int sock;
            //create socket
            sock = socket(AF_INET, SOCK_STREAM, 0);

            //configure settings in address struct
            server.sin_family = AF_INET;
            server.sin_port = htons(i);
            server.sin_addr.s_addr = inet_addr(ip.c_str()); //inet_addr converts string to long format ipV4 address

            //test if port is open
            res = connect(sock, (sockaddr * ) & server, sizeof(server));

#if defined(__linux__) || defined(__APPLE__)
            close(sock);
#elif defined(_WIN32)
            closesocket(sock);
#endif
            findPort = (res != 0);
            if (findPort) return i;
            i++;
        }
        return 0;
    }
*/


    string CrowSettings::_checkHostname(const string& name) {
        char ip[INET_ADDRSTRLEN];
        struct addrinfo hints{}, *res;
        //initialise struct at 0
        memset(&hints, 0, sizeof hints);
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;

        int status = getaddrinfo(name.c_str(), nullptr, &hints, &res);
        if (status != 0) {
            m_optionsData->setNumError(4);
            m_optionsData->setDetailsErrors(gai_strerror(status));
            return "";
        }

        auto *ipv4 = (struct sockaddr_in *) res->ai_addr;
        inet_ntop(res->ai_family, &(ipv4->sin_addr), ip, sizeof ip);
        freeaddrinfo(res);
        return (string) ip;
    }

    void CrowSettings::_ipV4ToTab(string name, int tab[], const int &arrayLength) {
        for(int i=0; i<arrayLength; i++){
            size_t p = name.find('.');
            tab[i] = stoi(name.substr(0,p));
            if (i<3) name.erase(0,p+1);
        }
    }

    bool CrowSettings::_isIpList(string name){
        int src[4] {0};
        int arrayLength = sizeof(src) / sizeof(src[0]);
        _ipV4ToTab(std::move(name), src, arrayLength);

        for (auto &ip : m_ips) {
            int dest[4] {0};
            _ipV4ToTab(ip, dest, arrayLength);
            if (src[0] == dest[0] && src[1] == dest[1] && src[2] == dest[2] && src[3] == dest[3]) {
                return true;
            }
        }
    return false;
    }


}