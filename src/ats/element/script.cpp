#include "../../../include/ats/element/script.hpp"

namespace ld {

    bool ElementScript::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "id", m_elementScriptData.m_id);
            CrowJsonResponse::jsonParse(value, "script", m_elementScriptData.m_script);
            return true;
        }
        return false;
    }

    bool ElementScript::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_elementScriptData.m_id);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_elementScriptData.m_script);
        return true;
    }
}
