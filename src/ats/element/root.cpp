#include "../../../include/ats/element/root.hpp"

namespace ld {

    bool ElementRoot::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "id", m_elementRootData.m_id);
            return true;
        }
        return false;
    }

    bool ElementRoot::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_elementRootData.m_id);
        return true;
    }
}
