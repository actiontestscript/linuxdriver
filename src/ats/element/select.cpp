#include "../../../include/ats/element/select.hpp"

namespace ld {

    bool ElementSelect::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "id", m_elementSelectData.m_id);
            CrowJsonResponse::jsonParse(value, "type", m_elementSelectData.m_type);
            CrowJsonResponse::jsonParse(value, "value", m_elementSelectData.m_value);
            CrowJsonResponse::jsonParse(value, "regexp", m_elementSelectData.m_regexp);
            return true;
        }
        return false;
    }

    bool ElementSelect::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_elementSelectData.m_id);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_elementSelectData.m_type);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_elementSelectData.m_value);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_elementSelectData.m_regexp);
        return true;
    }
}
