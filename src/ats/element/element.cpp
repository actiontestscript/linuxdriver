#include "../../../include/ats/element/element.hpp"

namespace ld {

    bool Element::execute(const crow::request &req, crow::response &res) {
        parseData(req);
        DesktopResponse::getResponse(res,getResponseType());
        return true;
    }

    void Element::parseData(const crow::request &req) {
        const std::string& body = req.body;
        if(CrowJsonResponse::isJson(body)){
            if(!_parseJson(req))
                if(_parsePostData(req)) setResponseType(CrowJsonResponse::ResponseType::AMF);

        }
        else {
            _parsePostData(req);
            setResponseType(CrowJsonResponse::ResponseType::AMF);
        }
    }


}

