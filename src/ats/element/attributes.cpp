#include "../../../include/ats/element/attributes.hpp"

namespace ld {

    bool ElementAttributes::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "id", m_elementAttributesData.m_id);
            CrowJsonResponse::jsonParse(value, "attribute", m_elementAttributesData.m_attributes);
            return true;
        }
        return false;
    }

    bool ElementAttributes::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_elementAttributesData.m_id);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_elementAttributesData.m_attributes);
        return true;
    }
}
