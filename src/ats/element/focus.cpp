#include "../../../include/ats/element/focus.hpp"

namespace ld {

    bool ElementFocus::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "id", m_elementFocusData.m_id);
            return true;
        }
        return false;
    }

    bool ElementFocus::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_elementFocusData.m_id);
        return true;
    }
}
