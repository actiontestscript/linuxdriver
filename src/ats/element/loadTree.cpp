#include "../../../include/ats/element/loadTree.hpp"

namespace ld {

    bool ElementLoadTree::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "handle", m_elementLoadTreeData.m_handle);
            CrowJsonResponse::jsonParse(value, "pid", m_elementLoadTreeData.m_pid);
            return true;
        }
        return false;
    }

    bool ElementLoadTree::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_elementLoadTreeData.m_handle);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_elementLoadTreeData.m_pid);
        return true;
    }
}
