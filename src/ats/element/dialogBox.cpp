#include "../../../include/ats/element/dialogBox.hpp"

namespace ld {

    bool ElementDialogBox::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "pId", m_elementDialogBoxData.m_pId);
            return true;
        }
        return false;
    }

    bool ElementDialogBox::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_elementDialogBoxData.m_pId);
        return true;
    }
}
