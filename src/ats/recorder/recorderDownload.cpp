#include "../../../include/ats/recorder/recorderDownload.hpp"
namespace ld {
    bool RecorderDownload::execute(const crow::request &req, crow::response &res) {
        Session::RecorderInfo* recorder = m_session->getRecorder(m_sessionId);
        if(recorder != nullptr) {
            std::ifstream file(recorder->amfFullPathFileName, std::ios::binary);
            if(!file.is_open()) {
                m_crowJsonResponse.setMsgErrorCode("file_not_found", "","File not found");
                res.code = m_crowJsonResponse.m_errorCode;
                res.write(m_crowJsonResponse.jsonMsgError().dump());
                return false;
            }
            std::ostringstream file_content;
            file_content << file.rdbuf();
            file.close();
            res.set_header("Content-Type", "application/octet-stream");
            res.write(file_content.str());

            //delete file
            std::remove(recorder->amfFullPathFileName.c_str());

            //delete from session
            m_session->deleteRecorder(m_sessionId);

            return true;

        }

        return false;
    }

    bool RecorderDownload::_parseJson(const crow::request &req) {
        return true;
    }

    bool RecorderDownload::_parsePostData(const crow::request &req) {
        return true;
    }
}// namespace ld

