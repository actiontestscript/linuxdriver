#include "../../../include/ats/recorder/recorderStatus.hpp"
namespace ld {
    bool RecorderStatus::execute(const crow::request &req, crow::response &res) {
        if(!m_recorderOld) {
            //check if json is valid
            if (!_jsonIsValid(req, res)) return false;

            //parse json
            if (!_parseJson(req)) {
                m_crowJsonResponse.setMsgErrorCode("invalid_argument", "", "JSON key value is not valid or missing");
                res.code = m_crowJsonResponse.m_errorCode;
                res.write(m_crowJsonResponse.jsonMsgError().dump());
                return false;
            }
        }
        else _parsePostData(req);

        Session::RecorderInfo* recorder = m_session->getRecorder(m_sessionId);
        if(recorder != nullptr) {
            recorder->currentAction.error = m_recorderStatusData.error;
            recorder->currentAction.duration = m_recorderStatusData.duration;
        }

        getResponse(res);

        return true;
    }

    bool RecorderStatus::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json& value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "error", m_recorderStatusData.error);
            CrowJsonResponse::jsonParse(value, "duration", m_recorderStatusData.duration);
            //_jsonParseInt(value, "error", m_recorderStatusData.error);
            //_jsonParseLong(value, "duration", m_recorderStatusData.duration);
            setBrowserInfo(value);
        }
        else return false;
        return true;
    }

    bool RecorderStatus::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0 , m_recorderStatusData.error);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0L , m_recorderStatusData.duration);
        //_strParseInt(data, m_postDataDelimiter, 0 , m_recorderStatusData.error);
        //_strParseLong(data, m_postDataDelimiter, 0L , m_recorderStatusData.duration);
        setBrowserInfoOld(data);
        return true;
    }
}// namespace ld

