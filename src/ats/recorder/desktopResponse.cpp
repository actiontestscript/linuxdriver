#include "../../../include/ats/recorder/desktopResponse.hpp"
namespace ld {

    void AppData::initAmfDesktopWindowAppData() { setAmfObjectAppData(amf_desktopAppData); }

    void AppData::setAmfObjectAppData(AmfObject &obj) const {
        obj.addSealedProperty("version", amf::AmfString(m_version));
        obj.addSealedProperty("build", amf::AmfString(m_buildVersion));
        obj.addSealedProperty("path", amf::AmfString(m_Path));
        obj.addSealedProperty("name", amf::AmfString(m_name));
        obj.addSealedProperty("icon", amf::AmfByteArray(m_icon));
    }

    json AppData::serializeToJson() {
        json j;
        j["version"] = m_version;
        j["build"] = m_buildVersion;
        j["path"] = m_Path;
        j["name"] = m_name;
        j["icon"] = m_icon;
        return j;
    }

    void DesktopWindow::initAmfDesktopWindow() { setAmfObjectDesktopWindow(amf_desktopWindow); }

    void DesktopWindow::setAmfObjectDesktopWindow(amf::AmfObject &obj) const {
        obj.addSealedProperty("pid", amf::AmfInteger(pid));
        obj.addSealedProperty("handle", amf::AmfInteger(handle));
        obj.addSealedProperty("app", m_app.getAmfAppData());

    }

    amf::AmfItemPtr DesktopWindow::WindowsToAmfItem() const{
        amf::AmfObject obj;
        setAmfObjectDesktopWindow(obj);
        return amf::AmfItemPtr(obj);
    }

    json DesktopWindow::serializeToJson() {
        json j;
        j["pid"] = pid;
        j["handle"] = handle;
        j["app"] = m_app.serializeToJson();
        return j;
    }

    void DesktopData::initAmfDesktopData() { setAmfObjectDesktopData(amf_desktopData); }

    void DesktopData::setAmfObjectDesktopData(amf::AmfObject &obj) const {
        obj.addSealedProperty("data", amf::AmfString(getData() ));
        //obj.addSealedProperty("name", amf::AmfString(name));
        //obj.addSealedProperty("value", amf::AmfString(value));
    }

    amf::AmfItemPtr DesktopData::attributesToAmfItem() const{
        amf::AmfObject obj;
        setAmfObjectDesktopData(obj);
        return amf::AmfItemPtr(obj);
    }

    json DesktopData::serializeToJson() {
        json j;
        j["name"] = name;
        j["value"] = value;
        return j;
    }


    void AtsElement::initAmfAtsElement() {
        //attributes.initAmfDesktopData();
        setAmfObjectAtsElement(amf_atsElement);
    }

    void AtsElement::setAmfObjectAtsElement(amf::AmfObject &obj) const {
        obj.addSealedProperty("id", amf::AmfString(id));
        obj.addSealedProperty("tag", amf::AmfString(tag));
        obj.addSealedProperty("clickable", amf::AmfBool(clickable));
        obj.addSealedProperty("x", amf::AmfDouble(x));
        obj.addSealedProperty("y", amf::AmfDouble(y));
        obj.addSealedProperty("width", amf::AmfDouble(width));
        obj.addSealedProperty("height", amf::AmfDouble(height));
        obj.addSealedProperty("visible", amf::AmfBool(visible));
        obj.addSealedProperty("password", amf::AmfBool(password));
        obj.addSealedProperty("numChildren", amf::AmfInteger(numChildren));
        if( attributes.empty() )obj.addSealedProperty("attributes", amf::AmfNull());
        else obj.addSealedProperty("attributes",  amf::AmfArray(convertAttributesToArray(attributes)));
        if(children.empty())obj.addSealedProperty("elements", amf::AmfNull());
        else obj.addSealedProperty("elements",  amf::AmfArray(convertElementsToArray(children)));

//        obj.addSealedProperty("attributes", attributes.getAmfDesktopData());
//        obj.addSealedProperty("children",  amf::AmfVector(children));
    }

    amf::AmfItemPtr AtsElement::elementsToAmfItem() const{
        amf::AmfObject obj;
        setAmfObjectAtsElement(obj);
        return amf::AmfItemPtr(obj);
    }

    void DesktopResponse::initAmfDesktopResponse() {
        //windows.initAmfDesktopWindow();
        //data.initAmfDesktopData();
        //elements.initAmfAtsElement();
        setAmfObjectDesktopResponse(amf_desktopResponse);
    }

    void DesktopResponse::setAmfObjectDesktopResponse(amf::AmfObject &obj) const {
        obj.addSealedProperty("errorCode", amf::AmfInteger(errorCode));
        obj.addSealedProperty("errorMessage", amf::AmfString(errorMessage));
        if(image.empty()) obj.addSealedProperty("image", amf::AmfNull());

        if(windows.empty()) obj.addSealedProperty("windows", amf::AmfNull());
        else obj.addSealedProperty("windows",  amf::AmfArray(convertWindowsToArray(windows)));

        //obj.addSealedProperty("data", amf::AmfNull());

        if(attributes.empty())obj.addSealedProperty("attributes", amf::AmfNull());
        else obj.addSealedProperty("attributes",  amf::AmfArray(convertAttributesToArray(attributes)));
        if(elements.empty())obj.addSealedProperty("elements", amf::AmfNull());
        else obj.addSealedProperty("elements",  amf::AmfArray(convertElementsToArray(elements)));


        //AmfNull
        /*
        obj.addSealedProperty("errorCode", amf::AmfInteger(errorCode));
        obj.addSealedProperty("errorMessage", amf::AmfString(errorMessage));
        obj.addSealedProperty("window", windows.getAmfDesktopWindow());
        obj.addSealedProperty("data", data.getAmfDesktopData());
        obj.addSealedProperty("elements", elements.getAmfAtsElement());
        */
    }

    amf::AmfArray AtsElement::convertAttributesToArray(const std::vector<DesktopData> &attributes) {
        amf::AmfArray result;

        for(auto& a : attributes){
            amf::AmfItemPtr itemPtr = a.attributesToAmfItem();
            auto& obj = itemPtr.as<amf::AmfObject>();
            result.push_back(obj);
        }
        return result;
    }

    amf::AmfArray AtsElement::convertElementsToArray(const std::vector<AtsElement> &element) {
        amf::AmfArray result;

        for(auto& e : element){
            amf::AmfItemPtr itemPtr = e.elementsToAmfItem();
            auto& obj = itemPtr.as<amf::AmfObject>();
            result.push_back(obj);
        }
        return result;
    }

    json AtsElement::serializeToJson() {
        json j;
        j["id"] = id;
        j["tag"] = tag;
        j["clickable"] = clickable;
        j["x"] = x;
        j["y"] = y;
        j["width"] = width;
        j["height"] = height;
        j["visible"] = visible;
        j["password"] = password;
        j["numChildren"] = numChildren;
        std::vector<json> attributesJson;
        for(auto& a : attributes) attributesJson.push_back(a.serializeToJson());
        j["attributes"] = attributesJson;
        std::vector<json> childrenJson;
        for(auto& c : children) childrenJson.push_back(c.serializeToJson());
        j["children"] = childrenJson;
        return j;
    }

    amf::AmfArray DesktopResponse::convertWindowsToArray(const std::vector<DesktopWindow> &window) {
        amf::AmfArray result;

        for(auto& w : window){
            amf::AmfItemPtr itemPtr = w.WindowsToAmfItem();
            auto& obj = itemPtr.as<amf::AmfObject>();
            result.push_back(obj);
        }
        return result;
    }

    amf::AmfArray DesktopResponse::convertAttributesToArray(const std::vector<DesktopData> &attribute) {
        amf::AmfArray result;

        for(auto& a : attribute){
            amf::AmfItemPtr itemPtr = a.attributesToAmfItem();
            auto& obj = itemPtr.as<amf::AmfObject>();
            result.push_back(obj);
        }
        return result;
    }

    amf::AmfArray DesktopResponse::convertElementsToArray(const std::vector<AtsElement> &element) {
        amf::AmfArray result;

        for(auto& e : element){
            amf::AmfItemPtr itemPtr = e.elementsToAmfItem();
            auto& obj = itemPtr.as<amf::AmfObject>();
            result.push_back(obj);
        }
        return result;
    }



    void DesktopResponse::getResponse(crow::response &res,const CrowJsonResponse::ResponseType& responseType) {
        DesktopResponse desktopResponse;
        getResponse(res,desktopResponse,responseType);
    }

    void DesktopResponse::getResponse(crow::response &res,DesktopResponse &desktopResponse,const CrowJsonResponse::ResponseType& responseType) {
        if(responseType == CrowJsonResponse::ResponseType::JSON){
            CrowJsonResponse::getJsonHeaderResponse(res);
            json j;
            j["value"] = desktopResponse.serializeToJson();
            res.code = 200;
            res.write(j.dump());
            return;
        }
        else {
            desktopResponse.initAmfDesktopResponse();
            amf::v8 resAmf = serialiseAmfActions(desktopResponse.getAmfDesktopResponse());
            res.set_header("Content-Type", "application/x-amf");
            std::string resAmfStr(reinterpret_cast<const char *>(resAmf.data()), resAmf.size());
            res.write(resAmfStr);
        }
    }

    json DesktopResponse::serializeToJson() {
        json j;
        j["errorCode"] = errorCode;
        j["errorMessage"] = errorMessage;
        if(image.empty()) j["image"] = nullptr;
        else j["image"] = image;

        if(windows.empty()) j["windows"] = nullptr;
        else {
            std::vector<json> windowsJson;
            for(auto& w : windows) windowsJson.push_back(w.serializeToJson());
            j["windows"] = windowsJson;
        }
        if(attributes.empty()) j["attributes"] = nullptr;
        else {
            std::vector<json> attributesJson;
            for (auto &a: attributes) attributesJson.push_back(a.serializeToJson());
            j["attributes"] = attributesJson;
        }
        if(elements.empty()) j["elements"] = nullptr;
        else {
            std::vector<json> elementsJson;
            for (auto &e: elements) elementsJson.push_back(e.serializeToJson());
            j["elements"] = elementsJson;
        }
        return j;
    }


}// namespace ld
