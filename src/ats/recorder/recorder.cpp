#include "../../../include/ats/recorder/recorder.hpp"

#include <opencv2/opencv.hpp>

#include "../../../include/utils/imagePng.hpp"

namespace ld {
    bool Recorder::_jsonIsValid(const crow::request &req, crow::response &res) {
        if (const std::string &body = req.body; !m_crowJsonResponse.isValidJson(body)) {
            LD_LOG_INFO << m_crowJsonResponse.jsonMsgError().dump() << std::endl;
            res.code = m_crowJsonResponse.m_errorCode;
            res.write(m_crowJsonResponse.jsonMsgError().dump());
            return false;
        }
        return true;
    }

    void Recorder::_jsonParseInt(const json &value, const std::string &key, int &data) {
        if (value.find(key) != value.end()) {
            if (value[key].is_number()) {
                data = value[key];
            } else if (value[key].is_string()) {
                if (const std::string &dataStr = value[key]; Utils::isInt(dataStr)) {
                    data = std::stoi(dataStr);
                }
            }
        }
    }

    void Recorder::_jsonParseLong(const json &value, const std::string &key, long &data) {
        if (value.find(key) != value.end()) {
            if (value[key].is_number()) {
                data = value[key];
            } else if (value[key].is_string()) {
                if (const std::string &dataStr = value[key]; Utils::isLong(dataStr)) {
                    data = std::stol(dataStr);
                }
            }
        }
    }

    void Recorder::_jsonParseDouble(const json &value, const std::string &key, double &data) {
        if (value.find(key) != value.end()) {
            if (value[key].is_number()) {
                data = value[key];
            } else if (value[key].is_string()) {
                if (const std::string &dataStr = value[key]; Utils::isDouble(dataStr)) {
                    data = std::stod(dataStr);
                }
            }
        }
    }

    void Recorder::_jsonParseString(const json &value, const std::string &key, std::string &data) {
        if (value.find(key) != value.end() && value[key].is_string()) {
            data = value[key];
        }
    }

    void Recorder::_jsonParseBool(const json &value, const std::string &key, bool &data) {
        if (value.find(key) != value.end() && value[key].is_boolean()) {
            data = value[key];
        } else if (value.find(key) != value.end() && value[key].is_string()) {
            if (const std::string &keyValue = value[key]; keyValue == "true") {
                data = true;
            } else if (keyValue == "false") {
                data = false;
            }
        }
    }

    void Recorder::flushFile(const std::string &filename, const v8 &data) {
        std::ofstream file;
        file.open(filename, std::ios::out | std::ios::app);
        if (file.is_open()) {
            if (data.size() > static_cast<std::size_t>(std::numeric_limits<std::streamsize>::max())) {
                throw std::overflow_error("Data size exceeds maximum supported size for std::streamsize.");
            }

            file.write(reinterpret_cast<const char *>(data.data()), static_cast<std::streamsize>(data.size()));
            file.close();
        }
    }

    void Recorder::flushVisualReport(const std::string &uuid) const {
        Session::RecorderInfo *recorder = m_session->getRecorder(uuid);
        recorder->currentReport.initAmfVisualReport();
        flushFile(recorder->amfFullPathFileName, serialiseAmfActions(recorder->currentReport.getAmfVisualReport()));
    }

    void Recorder::flushVisualAction(const std::string &uuid) const {
        Session::RecorderInfo *recorder = m_session->getRecorder(uuid);
        recorder->currentAction.initAmfVisualAction();
        flushFile(recorder->amfFullPathFileName, serialiseAmfActions(recorder->currentAction.getAmfVisualAction()));
    }

    void Recorder::flushReportSummary(const std::string &uuid) const {
        Session::RecorderInfo *recorder = m_session->getRecorder(uuid);
        recorder->currentSummary.initAmfTestSummary();
        flushFile(recorder->amfFullPathFileName, serialiseAmfActions(recorder->currentSummary.getAmfTestSummary()));
    }

    void Recorder::getResponse(crow::response &res) const {
        if (!m_recorderOld) {
            CrowJsonResponse::getJsonNullResponse(res);
        } else {
            DesktopResponse desktopResponse;
            desktopResponse.initAmfDesktopResponse();
            const v8 resAmf = serialiseAmfActions(desktopResponse.getAmfDesktopResponse());
            res.set_header("Content-Type", "application/x-amf");
            const std::string resAmfStr(reinterpret_cast<const char *>(resAmf.data()), resAmf.size());
            res.write(resAmfStr);
        }
    }

    void Recorder::_strParseInt(std::string &strResearch, const char &delimiter, const int &defaultValue, int &data) {
        size_t pos{};
        pos = strResearch.find(delimiter);
        if (const std::string dataStr = strResearch.substr(0, pos); Utils::isInt(dataStr)) {
            data = std::stoi(dataStr);
        } else {
            data = defaultValue;
        }
        strResearch.erase(0, pos + 1);
    }

    void Recorder::_strParseLong(std::string &strResearch, const char &delimiter, const long &defaultValue, long &data) {
        size_t pos{};
        pos = strResearch.find(delimiter);
        if (const std::string dataStr = strResearch.substr(0, pos); Utils::isLong(dataStr)) {
            data = std::stol(dataStr);
        } else {
            data = defaultValue;
        }
        strResearch.erase(0, pos + 1);
    }

    void Recorder::_strParseDouble(std::string &strResearch, const char &delimiter, const double &defaultValue, double &data) {
        size_t pos{};
        pos = strResearch.find(delimiter);
        if (const std::string dataStr = strResearch.substr(0, pos); Utils::isDouble(dataStr)) {
            data = std::stod(dataStr);
        } else {
            data = defaultValue;
        }
        strResearch.erase(0, pos + 1);
    }

    void Recorder::_strParseString(std::string &strResearch, const char &delimiter, const std::string &defaultValue, std::string &data) {
        size_t pos{};
        pos = strResearch.find(delimiter);
        if (const std::string dataStr = strResearch.substr(0, pos); !dataStr.empty()) {
            data = dataStr;
        } else {
            data = defaultValue;
        }
        strResearch.erase(0, pos + 1);
    }

    void Recorder::_strParseBool(std::string &strResearch, const char &delimiter, const bool &defaultValue, bool &data) {
        size_t pos{};
        pos = strResearch.find(delimiter);
        if (const std::string dataStr = strResearch.substr(0, pos); dataStr == "true") {
            data = true;
        } else if (dataStr == "false") {
            data = false;
        } else {
            data = defaultValue;
        }
        strResearch.erase(0, pos + 1);
    }

    void Recorder::setBrowserInfo(const json &value) {
        if (value.find("driverInfo") != value.end()) {
            const json &driverInfo = value["driverInfo"];
            CrowJsonResponse::jsonParse(driverInfo, "sessionId", m_browserSessionId);
            CrowJsonResponse::jsonParse(driverInfo, "screenshotUrl", m_browserScreenShotUrl);
            CrowJsonResponse::jsonParse(driverInfo, "headless", m_browserHeadless);
        }
    }

    void Recorder::setBrowserInfoOld(std::string value) {
        CrowJsonResponse::strParse(value, m_postDataDelimiter, std::string(""), m_browserSessionId);
        CrowJsonResponse::strParse(value, m_postDataDelimiter, std::string(""), m_browserScreenShotUrl);
        CrowJsonResponse::strParse(value, m_postDataDelimiter, false, m_browserHeadless);
    }

    void Recorder::addImage(VisualAction &objVisual) const {
        try {
            const std::vector<uint8_t> png_data = Utils::base64_decode(HttpRequest::getScreenShot(m_browserScreenShotUrl));
            /* if (png_data.empty()) {
                ImagePng img;
                ImagePng::createEmptyPNGImage(img, 3, 3);
                png_data = ImagePng::pngImageToBytes(img);
            } */

            cv::Mat image = imdecode(png_data, cv::IMREAD_COLOR);
            if (image.empty()) {
                return;
            }

            const int x = static_cast<int>(objVisual.element.rectangle.x);
            const int y = static_cast<int>(objVisual.element.rectangle.y);
            const int w = static_cast<int>(objVisual.element.rectangle.width);
            const int h = static_cast<int>(objVisual.element.rectangle.height);

            /* double offsetX, offsetY;
            getWebDriverXY(m_browserScreenShotUrl, offsetX, offsetY, m_browserHeadless);

            const cv::Rect rect(x - offsetX, y - offsetY, w, h); */

            const cv::Rect rect(x - 648, y - 548, w, h);
            rectangle(image, rect, cv::Scalar(211, 0, 148), 6);

            std::vector<uint8_t> output_data;
            if (const bool success = imencode(".webp", image, output_data, {cv::IMWRITE_WEBP_QUALITY, 25}); !success) {
                return;
            }

            objVisual.imageType = "webp";
            objVisual.images.emplace_back(output_data);
        } catch (const std::exception &e) {
            std::cerr << "Error: " << e.what() << "\n";
        }
    }

    void Recorder::getWebDriverXY(const std::string &webDriverUrlScreenShot, double &offsetX, double &offsetY, const bool &isHeadless) const {
        if (webDriverUrlScreenShot.empty()) {
            return;
        }

        std::string port{};
        const std::regex pattern(R"(^http[s]?:\/\/([\w\.-]+):(\d+)(/.*))");
        if (std::smatch match; std::regex_search(webDriverUrlScreenShot, match, pattern)) {
            port = match[2];
        }

        if (!port.empty()) {
            for (const auto &session : m_session->m_sessions) {
                if (session.isEnded || session.webdriverPort != port) {
                    continue;
                }

                offsetX = isHeadless ? session.webDriverHeadlessOffsetX : session.webDriverOffsetX;
                offsetY = isHeadless ? session.webDriverHeadlessOffsetY : session.webDriverOffsetY;
                return;
            }
        }
    }

    void Recorder::setOffsetWebDriver(const std::string &webDriverUrlScreenShot, Session::RecorderInfo &recorderInfo, const bool &isHeadless) const {
        if (!m_browserScreenShotUrl.empty()) {
            double offsetWebDriverX{0};
            double offsetWebDriverY{0};
            getWebDriverXY(webDriverUrlScreenShot, offsetWebDriverX, offsetWebDriverY, isHeadless);

            TestBound testBound;

            HttpRequest::getWebDriverGetWindowRect(m_browserScreenShotUrl, testBound.x, testBound.y, testBound.width, testBound.height);
            double widthInner{0};
            double heightInner{0};
            Utils::getPngSize(HttpRequest::getScreenShot(m_browserScreenShotUrl), widthInner, heightInner);
            recorderInfo.offsetPositionX = static_cast<int>(((testBound.width - widthInner) / 2) + offsetWebDriverX);
            recorderInfo.offsetPositionY = static_cast<int>(((testBound.height - heightInner) / 2) + offsetWebDriverY);
        } else {
            recorderInfo.offsetPositionX = 0;
            recorderInfo.offsetPositionY = 0;
        }
    }
}
