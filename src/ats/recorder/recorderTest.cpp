#include "../../../include/ats/recorder/recorderTest.hpp"
#include "../../../include/ats/recorder/testSummary.hpp"
#include "../../../include/ats/recorder/visualAction.hpp"
#include "../../../include/ats/recorder/visualReport.hpp"

namespace ld {
    amf::v8 RecorderTest::serialiseAmfActions(const amf::AmfObject& obj){
        Serializer serializer;
        serializer <<obj;
        amf::v8 data = serializer.data();
        return data;
    }


    bool RecorderTest::execute(const crow::request &req, crow::response &res) {
        /*
        std::vector<amf::v8> amfActions;
        VisualReport report;
        int index = 0;
        report.name = "test report";
        index++;
        report.index = index;
        report.initAmfVisualReport();
        amfActions.push_back(serialiseAmfActions(report.getAmfVisualReport()));

        VisualAction action;
        action.channelName = " channel mofifier click";
        index++;
        action.index=index;
        action.initAmfVisualAction();
        amfActions.push_back(serialiseAmfActions(action.getAmfVisualAction()));

        VisualAction action1;
        action1.channelName = " channel mofifier click";
        index++;
        action1.index=index;
        action1.initAmfVisualAction();
        amfActions.push_back(serialiseAmfActions(action1.getAmfVisualAction()));

        ReportSummary summary;
        summary.suiteName ="suite name modiifre";
        summary.testName = "test name modifier";
        summary.initAmfReportSummary();
        amfActions.push_back(serialiseAmfActions(summary.getAmfReportSummary()));




        std::ofstream output_file("amf3.atsv", std::ios::binary);
        if (output_file.is_open()) {
            for(auto amfAction : amfActions) {
             output_file.write((char *) amfAction.data(), amfAction.size());
            }
            output_file.close();
        }
*/

        //test amg array img
        class TestAmfArrayImg {
        private:
            amf::AmfObject amf_test = amf::AmfObject("com.ats.recorder.VisualAction", false, false);
        public:
            std::vector<std::vector<uint8_t>> images{};
            void setAmfTest(amf::AmfObject& obj) const{
                obj.addSealedProperty("images", convertImagesToAmfArray(images));
            };
            [[nodiscard]] amf::AmfObject getAmfTest() const{ return amf_test; };
            void initAmfTest(){
                setAmfTest(amf_test);
            };

        };

        TestAmfArrayImg testAmfArrayImg;
        TestAmfArrayImg testAmfArrayImg3;

        //test img png
        ImagePng img;
        ImagePng img1;
        ImagePng img2;
        int width = 100;
        int height = 200;
        img.createEmptyPNGImage(img, width, height);
        img.setAllPixelColor(255,0,255,255);
        testAmfArrayImg.images.push_back(img.pngImageToBytes(img));


        img1.createEmptyPNGImage(img1, width, height);
        img1.setAllPixelColor(255,0,0,255);
        testAmfArrayImg3.images.push_back(img1.pngImageToBytes(img1));
        testAmfArrayImg3.images.push_back(img.pngImageToBytes(img));

        img2.createEmptyPNGImage(img2, width, height);
        img2.setAllPixelColor(0,255,255,255);
        testAmfArrayImg3.images.push_back(img2.pngImageToBytes(img2));


        std::vector<amf::v8> amfTests;
        std::vector<amf::v8> amfTests3;

        testAmfArrayImg.initAmfTest();
        testAmfArrayImg3.initAmfTest();

        amfTests.push_back(serialiseAmfActions(testAmfArrayImg.getAmfTest()));
        amfTests3.push_back(serialiseAmfActions(testAmfArrayImg3.getAmfTest()));

        std::ofstream output_file("amf3_img_1.atsv", std::ios::binary);
        if (output_file.is_open()) {
            for(auto amfTest : amfTests) {
                output_file.write((char *) amfTest.data(), amfTest.size());
            }
            output_file.close();
        }

        std::ofstream output_file3("amf3_img_3.atsv", std::ios::binary);
        if (output_file3.is_open()) {
            for(auto amfTest3 : amfTests3) {
                output_file3.write((char *) amfTest3.data(), amfTest3.size());
            }
            output_file3.close();
        }


        /*
        res.set_header("Content-Disposition", "attachment; filename=image.png");
        res.set_header("Content-Type", "image/png");
        //std::string pngStr(reinterpret_cast<const char*>(resAmf.data()), resAmf.size());
        std::string pngStr(reinterpret_cast<const char*>(imgData.data()), imgData.size());
        res.write(pngStr);
        */
        return false;
    }

    bool RecorderTest::_parsePostData(const crow::request &req) {
        return true;
    }
}//namespace ld
