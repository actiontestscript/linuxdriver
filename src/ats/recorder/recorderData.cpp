#include "../../../include/ats/recorder/recorderData.hpp"
namespace ld{
    bool RecorderData::execute(const crow::request &req, crow::response &res) {
        if(!m_recorderOld) {
            //check if json is valid
            if (!_jsonIsValid(req, res)) return false;

            //parse json
            if (!_parseJson(req)) {
                m_crowJsonResponse.setMsgErrorCode("invalid_argument", "", "JSON key value is not valid or missing");
                res.code = m_crowJsonResponse.m_errorCode;
                res.write(m_crowJsonResponse.jsonMsgError().dump());
                return false;
            }
        }
        else _parsePostData(req);

        Session::RecorderInfo* recorder = m_session->getRecorder(m_sessionId);
        if(recorder != nullptr) {
            recorder->currentAction.value = m_recorderDataData.v1;
            recorder->currentAction.data = m_recorderDataData.v2;
        }

        getResponse(res);

        return true;
    }

    bool RecorderData::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json& value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "v1", m_recorderDataData.v1);
            CrowJsonResponse::jsonParse(value, "v2", m_recorderDataData.v2);
            //_jsonParseString(value, "v1", m_recorderDataData.v1);
            //_jsonParseString(value, "v2", m_recorderDataData.v2);
            setBrowserInfo(value);
        }
        else return false;
        return true;
    }

    bool RecorderData::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_recorderDataData.v1);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_recorderDataData.v2);
        //_strParseString(data, m_postDataDelimiter,"", m_recorderDataData.v1);
        //_strParseString(data, m_postDataDelimiter,"", m_recorderDataData.v2);
        setBrowserInfoOld(data);
        return false;
    }
}

