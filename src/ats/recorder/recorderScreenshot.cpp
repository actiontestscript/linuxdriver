#include "../../../include/ats/recorder/recorderScreenshot.hpp"
namespace ld {
    bool RecorderScreenshot::execute(const crow::request &req, crow::response &res) {
        if(!m_recorderOld) {
            //check if json is valid
            if (!_jsonIsValid(req, res)) return false;

            //parse json
            if (!_parseJson(req)) {
                m_crowJsonResponse.setMsgErrorCode("invalid_argument", "", "JSON key value is not valid or missing");
                res.code = m_crowJsonResponse.m_errorCode;
                res.write(m_crowJsonResponse.jsonMsgError().dump());
                return false;
            }
        }
        else _parsePostData(req);

        getResponse(res);
        return true;
    }

    bool RecorderScreenshot::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json& value = jsonBody["value"];
            _jsonParseInt(value, "x", m_recorderScreenshotData.x);
            _jsonParseInt(value, "y", m_recorderScreenshotData.y);
            _jsonParseInt(value, "w", m_recorderScreenshotData.width);
            _jsonParseInt(value, "h", m_recorderScreenshotData.height);
            setBrowserInfo(value);
        }
        else return false;
        return true;
    }

    bool RecorderScreenshot::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        _strParseInt(data, m_postDataDelimiter, 0, m_recorderScreenshotData.x);
        _strParseInt(data, m_postDataDelimiter, 1, m_recorderScreenshotData.y);
        _strParseInt(data, m_postDataDelimiter, 2, m_recorderScreenshotData.width);
        _strParseInt(data, m_postDataDelimiter, 3, m_recorderScreenshotData.height);
        setBrowserInfoOld(data);
        return true;
    }
}// namespace ld
