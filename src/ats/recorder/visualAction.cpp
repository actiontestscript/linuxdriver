#include "../../../include/ats/recorder/visualAction.hpp"
namespace ld {
    void VisualAction::setAmfObjectVisualAction(amf::AmfObject &obj) const {
        if (isVisualElement) obj.addSealedProperty("element", element.getAmfVisualElement());
        else obj.addSealedProperty("element", amf::AmfNull());
        if(isTestBound) obj.addSealedProperty("channelBound", channelBound.getAmfTestBound());
        else obj.addSealedProperty("channelBound", amf::AmfNull());
        obj.addSealedProperty("script", amf::AmfString(script));
        obj.addSealedProperty("stop", amf::AmfBool(stop));
        obj.addSealedProperty("channelName", amf::AmfString(channelName));
        obj.addSealedProperty("data", amf::AmfString(data));
        //obj.addSealedProperty("record", amf::AmfByteArray(record));
        obj.addSealedProperty("record", amf::AmfNull());
        obj.addSealedProperty("images", convertImagesToAmfArray(images));
        obj.addSealedProperty("imageType", amf::AmfString(imageType));
        obj.addSealedProperty("index", amf::AmfInteger(index));
        obj.addSealedProperty("line", amf::AmfInteger(line));
        obj.addSealedProperty("error", amf::AmfInteger(error));
        obj.addSealedProperty("duration", amf::AmfDouble(duration));
        obj.addSealedProperty("timeLine", amf::AmfDouble(timeLine));
        obj.addSealedProperty("type", amf::AmfString(type));
        obj.addSealedProperty("value", amf::AmfString(value));
        obj.addSealedProperty("imageRef", amf::AmfInteger(imageRef));

    }

    void VisualAction::initAmfVisualAction() {
        //record.emplace_back(0);
        channelBound.initAmfTestBound();isTestBound = true;
        //if(!TestBound::isDefault(channelBound)) {channelBound.initAmfTestBound();isTestBound = true; }
        if(!VisualElement::isDefault(element)) {
            element.isTestBoundApply = isTestBound;
            element.initAmfVisualElement();isVisualElement = true;
        }
        setAmfObjectVisualAction(amf_visualAction);
    }

    VisualAction::VisualAction(bool stop, const std::string& type, const int& line, std::string& script, const long &timeLine, std::string& channelName, const ld::TestBound& channelBound, const std::string& imageType){
        this->stop = stop;
        this->type = type;
        this->line = line;
        this->script = script;
        this->timeLine = timeLine;
        this->channelName = channelName;
        this->channelBound = channelBound;
        this->imageType = imageType;
        this->record.emplace_back(0);
    }

    VisualAction::VisualAction(const std::string& name){
        this->channelBound = TestBound(0,0,10,10);
        this->script = name;
        this->type = START_SCRIPT;
        this->record.emplace_back(0);
    }
}

