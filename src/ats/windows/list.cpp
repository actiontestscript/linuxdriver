#include "../../../include/ats/windows/list.hpp"
namespace ld{
    WindowsList::WindowsList() = default;

    bool WindowsList::execute(const crow::request &req, crow::response &res) {
        DesktopResponse desktopResponse;
        parseData(req);

/*
        std::vector<std::string> sessionIds{};
        m_session = Session::getInstance();
        if( m_session->m_processes.empty() ) return false;
        getBrowserPids();
        for(auto &process : m_session->m_processes){
            if( !process.second.isFirstStart ){
                std::string url = process.second.url;
                url += DIRSEPARATOR;
                url += "sessions";
                sessionIds = HttpRequest::getWebDriverIdSession(url);
                if( !sessionIds.empty() ) {
                    for (auto &sessionId: sessionIds) {
                        AtsElement elements;
                        elements.id = sessionId;
                        desktopResponse.elements.push_back(elements);
                    }
                }
                continue;
            }
        }
        */
        DesktopResponse::getResponse(res,desktopResponse,getResponseType());
        return true;
    }

    //std::vector<std::pair<std::string,std::string>> WindowsList::getWebDriverSessionPid() {
    void WindowsList::getWebDriverSessionPid() {
        m_session = Session::getInstance();
        for(auto &process : m_session->m_processes){
            if( !process.second.isFirstStart ){
                std::string pid = Session::getProcessPid(process.second);
                if( !pid.empty() ) m_windowsListData.m_nameWebDriverProcessAndPids.emplace_back(process.first, pid);
                continue;
            }
        }
    }

    void WindowsList::getBrowserPids(){
        getWebDriverSessionPid();
        if( m_windowsListData.m_nameWebDriverProcessAndPids.empty() ) return;
        for(auto &process : m_windowsListData.m_nameWebDriverProcessAndPids){
            std::string cmd = "pstree -p " + process.second + " | awk -F\"---\" '{print $2}' | awk -F\"(\" '{print $2}' | awk -F\")\" '{print $1}'";
            std::string result{};
            std::regex patternPid(R"(\d+)");
            std::smatch match;
            if(Utils::exec(cmd.c_str(),result)){
                while(std::regex_search(result, match, patternPid)){
//                    if( !match.empty() ) m_nameBrowserPids.emplace_back(process.first, match[0]);
                    if( !match.empty() ) m_windowsListData.m_nameBrowserPids[process.first].push_back(match[0]);
                    result = match.suffix();
                }
            }
        }
    }

    bool WindowsList::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "pid", m_windowsListData.m_pid);
            return true;
        }
        return false;
    }

    bool WindowsList::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowsListData.m_pid);
        return true;
    }

}