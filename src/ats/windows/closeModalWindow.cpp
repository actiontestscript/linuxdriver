#include "../../../include/ats/windows/closeModalWindow.hpp"
namespace ld{
    WindowsCloseModalWindow::WindowsCloseModalWindow() = default;

    bool WindowsCloseModalWindow::execute(const crow::request &req, crow::response &res) {
        parseData(req);
        DesktopResponse::getResponse(res,getResponseType());
        return true;
    }

    bool WindowsCloseModalWindow::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "handle", m_windowsCloseModalWindowData.m_handle);
            return true;
        }
        return false;
    }

    bool WindowsCloseModalWindow::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowsCloseModalWindowData.m_handle);
        return true;
    }
}