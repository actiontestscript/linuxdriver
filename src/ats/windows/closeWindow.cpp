#include "../../../include/ats/windows/closeWindow.hpp"
namespace ld{
    WindowsCloseWindow::WindowsCloseWindow() = default;

    bool WindowsCloseWindow::execute(const crow::request &req, crow::response &res) {
        parseData(req);
        DesktopResponse::getResponse(res,getResponseType());
        return true;
    }

    bool WindowsCloseWindow::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "handle", m_windowsCloseWindowData.m_handle);
            return true;
        }
        return false;
    }

    bool WindowsCloseWindow::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowsCloseWindowData.m_handle);
        return true;
    }
}