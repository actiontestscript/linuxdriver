#include "../../../include/ats/windows/windows.hpp"
namespace ld{
    void Windows::parseData(const crow::request &req) {
        const std::string& body = req.body;
        if(CrowJsonResponse::isJson(body)){
            if(!_parseJson(req))
                if(_parsePostData(req))
                    this->setResponseType(CrowJsonResponse::ResponseType::AMF);

        }
        else {
            _parsePostData(req);
            this->setResponseType(CrowJsonResponse::ResponseType::AMF);
        }
    }
}

