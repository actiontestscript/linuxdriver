#include "../../../include/ats/windows/state.hpp"
namespace ld{
    WindowsState::WindowsState() = default;

    bool WindowsState::execute(const crow::request &req, crow::response &res) {
        parseData(req);
        DesktopResponse::getResponse(res,getResponseType());
        return true;
    }


    bool WindowsState::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "handle", m_windowsStateData.m_handle);
            CrowJsonResponse::jsonParse(value, "state", m_windowsStateData.m_state);
            return true;
        }
        return false;
    }

    bool WindowsState::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowsStateData.m_handle);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_windowsStateData.m_state);
        return true;
    }
}