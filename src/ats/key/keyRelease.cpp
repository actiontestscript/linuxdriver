#include "../../../include/ats/key/keyRelease.hpp"
namespace ld{
    bool KeyRelease::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "codePoint", m_KeyReleaseData.m_codePoint);
            return true;
        }
        return false;
    }

    bool KeyRelease::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, -1, m_KeyReleaseData.m_codePoint);

        return true;
    }

} //end namespace ld
