#include "../../../include/ats/key/keyClear.hpp"
namespace ld{
    bool KeyClear::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "id", m_keyClearData.m_id);
            return true;
        }
        return false;
    }

    bool KeyClear::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_keyClearData.m_id);
        return true;
    }

} //end namespace ld