#include "../../../include/ats/key/keyEnter.hpp"
namespace ld{
    bool KeyEnter::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "id", m_KeyEnterData.m_id);
            CrowJsonResponse::jsonParse(value, "data", m_KeyEnterData.m_data);
            return true;
        }
        return false;
    }

    bool KeyEnter::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_KeyEnterData.m_id);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_KeyEnterData.m_data);
        return true;
    }

} //end namespace ld
