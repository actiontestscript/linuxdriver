#include "../../../include/ats/driver/driverShapes.hpp"
namespace ld {

    bool DriverShapes::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "crop", m_DriverShapesData.m_crop);
            CrowJsonResponse::jsonParse(value, "device", m_DriverShapesData.m_device);
            CrowJsonResponse::jsonParse(value, "duration", m_DriverShapesData.m_duration);
            return true;
        }
        return false;
    }

    bool DriverShapes::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        std::string crop_x = 0;
        std::string crop_y = 0;
        std::string crop_w = 0;
        std::string crop_h = 0;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_DriverShapesData.m_duration);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_DriverShapesData.m_device);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), crop_x);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), crop_y);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), crop_w);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), crop_h);
        m_DriverShapesData.m_crop = crop_x + "," + crop_y + "," + crop_w + "," + crop_h;
        return true;
    }
}
