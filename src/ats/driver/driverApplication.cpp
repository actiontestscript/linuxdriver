#include "../../../include/ats/driver/driverApplication.hpp"
namespace ld {

    bool DriverApplication::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            std::string args{};
            CrowJsonResponse::jsonParse(value, "args", args);
            while(!args.empty()){
                std::string argument;
                if(args.find('\n') != std::string::npos)
                    CrowJsonResponse::strParse(args, '\n', std::string(""), argument);
                else {
                    argument = args;
                    args.clear();
                }
                m_DriverApplicationData.m_args.push_back(argument);
            }
//            CrowJsonResponse::jsonParse(value, "args", m_DriverApplicationData.m_args);
            CrowJsonResponse::jsonParse(value, "attach", m_DriverApplicationData.m_attach);
            return true;
        }
        return false;
    }

    bool DriverApplication::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, false, m_DriverApplicationData.m_attach);
        while(!data.empty()){
            std::string argument;
            if(data.find('\n') != std::string::npos)
                CrowJsonResponse::strParse(data, '\n', std::string(""), argument);
            else {
                argument = data;
                data.clear();
            }
            m_DriverApplicationData.m_args.push_back(argument);
        }
        return true;
    }
}
