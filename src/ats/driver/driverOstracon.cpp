#include "../../../include/ats/driver/driverOstracon.hpp"
namespace ld {

    bool DriverOstracon::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "handle", m_DriverOstraconData.m_handle);
            return true;
        }
        return false;
    }

    bool DriverOstracon::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, -1, m_DriverOstraconData.m_handle);
        return true;
    }
}


