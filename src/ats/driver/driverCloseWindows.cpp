#include "../../../include/ats/driver/driverCloseWindows.hpp"
namespace ld {

    bool DriverCloseWindows::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "processId", m_DriverCloseWindowsData.m_processId);
            CrowJsonResponse::jsonParse(value, "handle", m_DriverCloseWindowsData.m_handle);
            return true;
        }
        return false;
    }

    bool DriverCloseWindows::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0L, m_DriverCloseWindowsData.m_processId);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, -1, m_DriverCloseWindowsData.m_handle);
        return true;
    }
}

