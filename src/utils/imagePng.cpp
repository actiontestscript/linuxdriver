#include <opencv2/opencv.hpp>
#include <vector>

namespace ld {

class ImagePng {
public:
    ImagePng() : data(), width(0), height(0) {}

    std::vector<unsigned char> pngImageToBytes(const ImagePng& img) {
        std::vector<unsigned char> output;
        if (img.data.empty()) return output;

        // Encode the image to PNG format
        std::vector<int> compression_params = {cv::IMWRITE_PNG_COMPRESSION, 3}; // Compression level 0-9
        cv::imencode(".png", img.data, output, compression_params);

        return output;
    }

    std::vector<unsigned char> pngImageToBytes(ImagePng& img, const int& w, const int& h) {
        if (img.data.empty()) createEmptyPNGImage(img, w, h);
        return pngImageToBytes(img);
    }

    void setPixelColor(const int& x, const int& y, const int& red, const int& green, const int& blue, const int& alpha) {
        if (x >= 0 && x < width && y >= 0 && y < height) {
            auto& pixel = data.at<cv::Vec4b>(y, x); // Access RGBA pixel
            pixel[0] = blue;  // OpenCV uses BGR(A) format
            pixel[1] = green;
            pixel[2] = red;
            pixel[3] = alpha;
        }
    }

    void setAllPixelColor(const int& red, const int& green, const int& blue, const int& alpha) {
        if (data.empty()) return;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                setPixelColor(x, y, red, green, blue, alpha);
            }
        }
    }

    void setColorType(int colorType) {
        // OpenCV primarily uses BGR(A), so no equivalent color type logic needed here
        // If additional functionality is required, you can implement it.
    }

private:
    void createEmptyPNGImage(ImagePng& img, const int& w, const int& h) {
        img.width = w;
        img.height = h;
        img.data = cv::Mat(h, w, CV_8UC4, cv::Scalar(0, 0, 0, 255)); // Create blank RGBA image
    }

    cv::Mat data; // Stores the image data in OpenCV format
    int width;
    int height;
};

} // namespace ld