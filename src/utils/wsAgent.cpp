#include "../../include/utils/wsAgent.hpp"

namespace ld{
    //static init
    bool WsAgent::m_isConnected = false;

    WsAgent::WsAgent(int port) : server(port, "0.0.0.0"), isRunning(false) {
        server.setOnConnectionCallback([this](const std::weak_ptr<ix::WebSocket>& webSocket, const std::shared_ptr<ix::ConnectionState>& connectionState) {
            std::lock_guard<std::mutex> lock(connectionMutex);
            auto ws = webSocket.lock();
            if(ws){
                ws->setOnMessageCallback([webSocket,connectionState, this](const ix::WebSocketMessagePtr& msg) {
                    if(msg->type == ix::WebSocketMessageType::Open){
                        Connect(webSocket, connectionState);
                    }
                    else if (msg->type == ix::WebSocketMessageType::Message) {
                        LD_LOG_DEBUG << "Nouveau message: " << msg->str << std::endl;
                        Update(webSocket, connectionState, msg->str);
                    }
                    else if (msg->type == ix::WebSocketMessageType::Close) {
                        //LD_LOG_DEBUG << "Close Connection: " << msg->str << std::endl;
                        DisConnect(webSocket,connectionState);
                    }
                });
            }
        });
    }

    WsAgent::~WsAgent() {  Stop(); }

    void WsAgent::Start() {
        isRunning = true;
        serverThread = std::thread(&WsAgent::Run, this);
    }

    void WsAgent::Stop() {
        isRunning = false;
        cv.notify_one();
        if (serverThread.joinable()) {
            serverThread.join();
        }
        SetWebSocketConnected(false);
    }

    void WsAgent::Connect(const std::weak_ptr<ix::WebSocket>& webSocket , const std::shared_ptr<ix::ConnectionState> &connectionState) {
        //LD_LOG_DEBUG << "Nouvelle connexion" << std::endl;
        std::lock_guard<std::mutex> lock(connectionMutex);
        if(!activeConnection.expired()){
        //    LD_LOG_DEBUG<< "Une seule connexion à la fois est autorisée" << std::endl;
            auto ws = webSocket.lock();
            if(ws){
                ws->close(1000, "");
            }
            return;
        }
        ClearTerminal();
        activeConnection = connectionState;
        activeWebSocket = webSocket;
        activeConnectionId = connectionState->getId();
        address = connectionState->getRemoteIp();
        usedBy = address;
        Session::setWsAgentUsedBy(usedBy);
        Session::setWsAgentIsConnected(true);


        SetWebSocketConnected(true);
        LD_LOG_DEBUG << "Connexion établie remote ip : "<< address << std::endl;

    }

    void WsAgent::DisConnect(const std::weak_ptr<ix::WebSocket> &webSocket,const std::shared_ptr<ix::ConnectionState> &connectionState) {
//        LD_LOG_DEBUG << "client disconnect"<<std::endl;
        if(connectionState && activeConnectionId == connectionState->getId()){
//            LD_LOG_DEBUG << "client autorisé deconnecté"<<std::endl;
            address = "";
            usedBy = "";
            Session::setWsAgentUsedBy(usedBy);
            Session::setWsAgentIsConnected(false);
            activeWebSocket.reset();
            activeConnection.reset();
            activeConnectionId="";
            SetWebSocketConnected(false);
            m_sessionDelete.deleteAllSession();
            Utils::killBrowserLaunchWithWebdriver();
        }
    }

    void WsAgent::Update(const std::weak_ptr<ix::WebSocket> &webSocket,const std::shared_ptr<ix::ConnectionState> &connectionState,const std::string& msg) {
//        LD_LOG_DEBUG << "Update "<<std::endl;
        auto ws = webSocket.lock();
        if(ws){
            if(msg =="isConnected"){
                bool isConnected = !activeConnection.expired() && activeConnectionId == connectionState->getId();
                std::string log = Logger::ATS_AGENT_CONNECTED + "|" + (isConnected ? "true" : "false");
                ws->send(log);
            }
            else {
//            LD_LOG_DEBUG << "Locked"<<std::endl;
                usedBy = msg;
                Session::setWsAgentUsedBy(usedBy);
                PrintLog(Logger::ATS_DRIVER_VERSION, driverVersion, false);
                LD_LOG_ALWAYS << "client connected : " << usedBy << "@" << address << std::endl;
            }
        }
    }


    void WsAgent::SetData(const std::string &systemDriverVersion, int webServerPort, int wsPort, bool local) {
        m_isLocal = local;
        port = wsPort;
        driverVersion = systemDriverVersion;
/*
        PrintLog(Logger::ATS_DRIVER_VERSION, driverVersion, !local);
        PrintLog(Logger::ATS_DRIVER_PORT, std::to_string(webServerPort), !local);
        PrintLog(Logger::ATS_AGENT_PORT, std::to_string(wsPort), !local);
*/
        Session::addMsgStartup(Logger::ATS_DRIVER_VERSION+"|"+driverVersion);
        Session::addMsgStartup(Logger::ATS_DRIVER_PORT+"|"+std::to_string(webServerPort));
        Session::addMsgStartup(Logger::ATS_AGENT_PORT+"|"+std::to_string(wsPort));
    }


    void WsAgent::PrintLog(const std::string &type, const std::string &data, bool silent) {
        PrintLog(type,data,false,silent);
    }

    void WsAgent::PrintLog(const std::string &type, const std::string &data, bool outputError, bool silent) {
        std::string log = type + "|" + data;

        try{
            SendLog(log);
        }
        catch (std::exception& e){
            LD_LOG_ERROR << "Exception : " << e.what() << std::endl;
        }

        if(!silent){
            if(!outputError) {
                LD_LOG_ALWAYS << log << std::endl;
                return;
            }
            LD_LOG_ERROR << log << std::endl;
        }
    }

    void WsAgent::LogInfo(const std::string &data, bool silent) { PrintLog(Logger::ATS_AGENT_INFO,data,silent); }
    void WsAgent::LogWarning(const std::string &data, bool silent) { PrintLog(Logger::ATS_AGENT_WARNING,data,silent); }
    void WsAgent::LogError(const std::string &data, bool silent) { PrintLog(Logger::ATS_AGENT_ERROR,data,true,silent); }

    void WsAgent::Run() {
        server.listen();
        server.start();
        std::unique_lock<std::mutex> lock(cvMutex);
        cv.wait(lock, [this] { return !isRunning; });
        server.stop();
    }

    void WsAgent::SetWebSocketConnected(bool isConnected){
        m_isConnected = isConnected;
    }

    void WsAgent::SendLog(const std::string &log) {
        if(m_isLocal){ return; }
        auto webSocket = activeWebSocket.lock();
        if(webSocket){
            auto conn = activeConnection.lock();
            if(conn){
                webSocket->send(log);
            }
        }
    }

    void WsAgent::ClearTerminal() {
        const char* term = std::getenv("TERM");
        try {
            if (!term) {
                setenv("TERM", "xterm", 1);
                std::system("clear");
            } else {
                std::system("clear");
            }
            for(auto &msg : Session::getMsgStartup()){
                LD_LOG_ALWAYS << msg << endl;
            }
        }
        catch (std::exception& e){
            LD_LOG_ERROR << "Exception : " << e.what() << std::endl;
        }

    }
}
