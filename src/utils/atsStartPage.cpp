#include "../../include/utils/atsStartPage.hpp"

namespace ld{

    std::string AtsStartPage::getHtmlPage() {


        return m_pageContent;

    }


    AtsStartPage::AtsStartPage(const crow::request& req) {
        OsData capabilities = m_osData.getData();
        std::string pageContent = m_pageContent;

        std::vector<std::string> keys{};
        std::unordered_map<std::string, std::string> dataMap{};
        keys = req.url_params.keys();
        for (auto &key : keys) {
            dataMap[key] = req.url_params.get(key);
        }


        pageContent = std::regex_replace(pageContent, std::regex("#MACHINE_NAME#"), capabilities.machineName);
        pageContent = std::regex_replace(pageContent, std::regex("#WINDOWSDRIVER_VERSION#"), capabilities.driverVersion);
        pageContent = std::regex_replace(pageContent, std::regex("#SYSTEM_NAME#"), capabilities.platformName);
        pageContent = std::regex_replace(pageContent, std::regex("#SYSTEM_VERSION#"), capabilities.platformVersion);
        pageContent = std::regex_replace(pageContent, std::regex("#BUILD_VERSION#"), capabilities.platformVersion);
        pageContent = std::regex_replace(pageContent, std::regex("#COUNTRY_CODE#"), capabilities.countryCode);
        pageContent = std::regex_replace(pageContent, std::regex("#DOTNET_VERSION#"), std::to_string(__cplusplus));
        pageContent = std::regex_replace(pageContent, std::regex("#SCREEN_RESOLUTION#"), capabilities.screenWidth + " x " + capabilities.screenHeight);
        pageContent = std::regex_replace(pageContent, std::regex("#PROCESSOR_NAME#"), capabilities.processorName);
        pageContent = std::regex_replace(pageContent, std::regex("#PROCESSOR_SOCKET#"), capabilities.processorSocket);
        pageContent = std::regex_replace(pageContent, std::regex("#PROCESSOR_ARCH#"), capabilities.processorArch);
        pageContent = std::regex_replace(pageContent, std::regex("#PROCESSOR_SPEED#"), capabilities.processorSpeed);
        pageContent = std::regex_replace(pageContent, std::regex("#PROCESSOR_CORES#"), capabilities.processorCoreNumber);
        pageContent = std::regex_replace(pageContent, std::regex("#CURRENT_DRIVE#"), capabilities.systemDriveMountPoint);
        pageContent = std::regex_replace(pageContent, std::regex("#DISK_SIZE#"), capabilities.systemDriveTotalSize);
        pageContent = std::regex_replace(pageContent, std::regex("#DISK_FREE#"), capabilities.systemDriveFreeSpace);
        if(dataMap.find("pd") != dataMap.end())
            pageContent = std::regex_replace(pageContent, std::regex("#PROFILE_DIR#"), dataMap["pd"]);
        if(dataMap.find("av") != dataMap.end())
            pageContent = std::regex_replace(pageContent, std::regex("#ATS_VERSION#"), dataMap["av"]);
        if(dataMap.find("tt") != dataMap.end())
            pageContent = std::regex_replace(pageContent, std::regex("#WIN_TITLE#"), dataMap["tt"]);
        if(dataMap.find("et") != dataMap.end())
            pageContent = std::regex_replace(pageContent, std::regex("#ELEMENT_MAX_TRY#"), dataMap["et"]);
        if(dataMap.find("pt") != dataMap.end())
            pageContent = std::regex_replace(pageContent, std::regex("#PROPERTY_MAX_TRY#"), dataMap["pt"]);
        if(dataMap.find("js") != dataMap.end())
            pageContent = std::regex_replace(pageContent, std::regex("#JS_EXEC_TIMEOUT#"), dataMap["js"]);
        if(dataMap.find("to") != dataMap.end())
            pageContent = std::regex_replace(pageContent, std::regex("#PAGE_LOAD_TIMEOUT#"), dataMap["to"]);
        if(dataMap.find("wd") != dataMap.end())
            pageContent = std::regex_replace(pageContent, std::regex("#WATCHDOG#"), dataMap["wd"]);
        if(dataMap.find("dv") != dataMap.end())
            pageContent = std::regex_replace(pageContent, std::regex("#DRIVER_VERSION#"), dataMap["dv"]);
        if(dataMap.find("bn") != dataMap.end())
            pageContent = std::regex_replace(pageContent, std::regex("#BROWSER_NAME#"), dataMap["bn"]);
        if(dataMap.find("bv") != dataMap.end())
            pageContent = std::regex_replace(pageContent, std::regex("#BROWSER_VERSION#"), dataMap["bv"]);
        if(dataMap.find("px") != dataMap.end() && dataMap.find("py") != dataMap.end())
            pageContent = std::regex_replace(pageContent, std::regex("#START_POSITION#"), dataMap["px"] + " x " + dataMap["py"]);
        if(dataMap.find("pw") != dataMap.end() && dataMap.find("ph") != dataMap.end())
            pageContent = std::regex_replace(pageContent, std::regex("#START_SIZE#"), dataMap["pw"] + " x " + dataMap["ph"]);
        if(dataMap.find("wa") != dataMap.end())
            pageContent = std::regex_replace(pageContent, std::regex("#WAIT_ACTION#"), dataMap["wa"]);
        if(dataMap.find("dc") != dataMap.end())
            pageContent = std::regex_replace(pageContent, std::regex("#DOUBLE_CHECK#"), dataMap["dc"]);


        m_pageContent = pageContent;
    }
}

