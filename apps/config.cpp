#include "config.hpp"
char* ld::Config::currentPath = nullptr;
char* ld::Config::tmpPath = nullptr;
char* ld::Config::argv0 = nullptr;
std::string ld::Config::logLevelAll {"INFO"};
std::string ld::Config::logType {"NONE"};
std::string ld::Config::logWebDriverLevel {"NONE"};
std::string ld::Config::logCrowCppLevel {"NONE"};
