#include <iostream>
#include <nlohmann/json.hpp>
#include <utility>
#include <sstream>
#include <vector>
#include <string>
#include <regex>

using json = nlohmann::json;

void print_keys(const json& j,std::vector<std::pair<std::string,std::string>>& keysTree,const size_t index = -1 , const std::string& parent_key = "",const int tabIndex =-1)
{
    std::vector<std::string> ordered_keys;

    if(j.empty()) return;

    for (auto it = j.begin(); it != j.end(); ++it) {
        ordered_keys.push_back(it.key());
    }
     for (const auto& key : ordered_keys) {
        size_t parent_index = index;
        const auto& element = j[key];
        if (parent_key.empty()){
            keysTree.emplace_back("",key);
            parent_index = keysTree.size()-1;

        }
        else {
            //if (parent_key != "capabilities" && key != "capabilities") {
            std::string parent="";
            if(tabIndex != -1) {
                parent=( (keysTree[index].first).empty() ? "": keysTree[index].first+".") + keysTree[index].second+"["+std::to_string(tabIndex) + "]";
            }
            else {
                parent=( (keysTree[index].first).empty() ? "": keysTree[index].first+ ".") + keysTree[index].second;
            }
            keysTree.emplace_back(parent,key);
            parent_index = keysTree.size()-1;
        }

        if(element.is_object() ){
        std::cout << "parent : " << keysTree[parent_index].first << " key : " << key << '\n';
        print_keys(element,keysTree,parent_index, key);
        }
        else if(element.is_array()){
             int nbElement = element.size();
                for (int i = 0; i < nbElement; ++i) {
                    std::cout << "parent : " << keysTree[parent_index].first << " key : " << key << '\n';
                    print_keys(element[i],keysTree,parent_index, key,i);
                }
         }
        else{

/*
            std::string property = keysTree[parent_index].first + "." + key;
            std::stringstream ss(property);
            std::string segment;
            json& current = const_cast<json &>(j);
            while(std::getline(ss, segment, '.'))
            {
                current = current[segment];
            }
*/

            std::cout << "parent : " << keysTree[parent_index].first << " key : " << key << " value : " << element << '\n';
        }
     }
}

int main() {
    // Définition du JSON
    json j = R"(
    {
        "capabilities": {
            "firstMatch": [
                {
                    "browserName": "firefox",
                    "browserVersion": "latest",
                    "platformName": "windows 10"
                },
                {
                    "browserName": "chrome",
                    "browserVersion": "latest",
                    "platformName": "windows 10"
                }
            ]
        }
}
    )"_json;

    // Affichage des clés
    std::vector<std::pair<std::string,std::string>> keys = {};
    print_keys(j,keys);


    std::string property = "capabilities.firstMatch.[0].browserName";
    std::stringstream ss(property);
    std::string segment;
    json current = j;
    while (std::getline(ss, segment, '.')) {
        std::regex pattern("^(?:\\[)[0-9]+(?:\\])$");
        if (std::regex_match(segment, pattern)) {
            segment = segment.substr(1, segment.size() - 2);
            current = current[std::stoi(segment)];
        }
        else{
            current = current[segment];
        }
        /*
        if (segment.find_first_not_of("0123456789") == std::string::npos) {
            current = current[std::stoi(segment)];
        } else {
            current = current[segment];
        }
         */
    }

    std::string browserName = current;
    std::cout << "Le browserName est : " << browserName << std::endl;

    std::cout<<"current dierct : " << j["capabilities"]["firstMatch"][0]["browserName"] << '\n';

    return 0;
}
/*
en c++, avec la librairie json nlohmann 3.11.2, avec le json suivant :
     json j = R"(
    {
        "capabilities": {
            "firstMatch": [
                {
                    "browserName": "firefox",
                    "browserVersion": "latest",
                    "platformName": "windows 10"
                },
                {
                    "browserName": "chrome",
                    "browserVersion": "latest",
                    "platformName": "windows 10"
                }
            ]
        }
}
    )"_json;

    j'ai un parametre capabilities.firstMatch.browserName, je voudrais recupere la valeur de browserName



/*
    json j = R"(
    {
"capabilities":
        {
            "name": "John Smith",
            "age": 30,
            "address": {
                "street": "123 Main Street",
                "city": "Anytown",
                "state": "CA",
                "zip": "12345"
            },
            "phoneNumbers": [
                {
                    "type": "home",
                    "number": "555-555-1234"
                },
                {
                    "type": "work",
                    "number": "555-555-5678"
                }
            ]
        },

"phone": [      {
                    "type": "home",
                    "number": "555-555-1234"
                },
                {
                    "type": "work",
                    "number": "555-555-5678"
                }
            ]
}
    )"_json;
 */






/*
 *
 en c++, avec la librairie json nlohmann 3.11.2, dans le json suivant je voudrais connaitre la cle parent de street :
     {
        "name": "John Smith",
        "age": 42,
        "isMarried": true,
        "hobbies": ["reading", "swimming", "gardening"],
        "address": {
            "street": "123 Main Street",
            "city": "Anytown",
            "state": "CA",
            "zip": "12345"
        }
    }
 */