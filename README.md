# driverlinux
## _WebDriver linux_

linuxdriver is a web driver for linux, it is based on the <a href="https://www.w3.org/TR/webdriver/" target="_blank">W3C WebDriver</a> project

## Features

## Tech
driverlinux uses a number of open source projects to work properly:
- <a href="https://crowcpp.org/master/" target="_blank">crowCpp</a> Crow is a C++ framework for creating HTTP or Websocket web services
- <a href="https://think-async.com/Asio/" target="_blank">AsioCpp</a> libraries asio cpp dependence de CrowCpp
- <a href="https://pugixml.org/" target="_blank">pugixml</a> pugixml is a light-weight C++ XML processing library
- <a href="https://github.com/catchorg/Catch2" target="_blank"> Catch2 </a> Catch2 is a multi-paradigm test framework for C++
- <a href="https://github.com/nlohmann/json" target="_blank"> nlohmann/json </a> JSON for Modern C++
- <a href="https://github.com/yhirose/cpp-httplib" target="_blank"> cpp-httplib </a> A C++ header-only HTTP/HTTPS server and client library
- <a href="https://github.com/glennrp/libpng" target="_blank"> libpng </a> libpng is the official PNG reference library

## Development
### - First install crowcpp with dependencies
- Ubuntu 22.04 LTS :
```sh
cd ~
sudo apt-get update
sudo apt-get install g++ cmake make git catch2
```
- Fedora 37 :
```sh
cd ~
sudo dnf update
sudo dnf install gcc-c++ cmake make git boost-devel catch2
```
### - Second clone src linuxdriver
```sh
git clone --recurse-submodules https://gitlab.com/actiontestscript/linuxdriver.git
cd linuxdriver
./clean.sh        # clean build directory
./configure.sh    # configure build directory
./build.sh        # build linuxdriver
./build.sh install # install linuxdriver in $HOME/.actiontestscript
./run.sh          # run linuxdriver
or 
./gen.sh         # run linuxdriver
```
the gen.sh script execute the following scripts 
- clean.sh      : clean build directory
- configure.sh  : configure build directory
- build.sh      : build linuxdriver
- build.sh install : install linuxdriver in $HOME/.actiontestscript
- run.sh        : run linuxdriver

## Settings
### directory structure
```sh
.actiontestscript
├── drivers/
│   └── linuxdriver
├── libs/
├── .atsProperties
```

### .atsProperties
this file is used to configure the linuxdriver. 
```sh
<execute>
<SystemDriver>
    <remoteHostname>192.168.0.46</remoteHostname>        <!-- optional hostname or ip of the remote machine -->
	<allowedIp>192.168.0.41</allowedIp>                       <!-- allow individual ip -->
	<allowedIp>192.168.0.42</allowedIp>                       <!-- another allow individual ip -->
	<allowedIpRange>                                          <!-- allow ip range -->
		<start>192.168.0.44</start>
		<end>192.168.0.46</end>
	</allowedIpRange>
	<allowedIpRange>                                          <!-- another allow ip range -->
		<start>192.168.1.10</start>
		<end>192.168.1.15</end>
	</allowedIpRange>
 </SystemDriver>
 </execute>
```

### Default network settings
#### Allowed ip
default allowed ip is localhost
#### Bind address
default bind address is localhost 127.0.0.1
#### Port
the autoconfiguration port range is 9700 to 9800
- default port is 9700 for linux driver
- default port is 9710 for chrome driver
- default port is 9710 for firefox gecko driver
- default port is 9710 for opera driver
- default port is 9710 for ms-edge driver


### For remote access to linuxdriver
#### settings on the remote machine
- install linuxdriver on the remote machine
- configure the .atsProperties file on the remote machine
- configure the .atsProperties file on the local machine
- configure firewall on the remote machine to allow access to the linuxdriver port
on Ubuntu 22.04 LTS :
```sh
sudo ufw allow 9700/tcp
sudo ufw allow 9710/tcp
#for range
sudo ufw allow 9700:9800/tcp
sudo ufw reload
#check
sudo ufw status
```


## Status
in development

## Support

## Roadmap

## Contributing

## Authors and acknowledgment
<a href="https://fr.agilitest.com/" target="_blank">Agilitest</a>


## License
OpenSource apache

[crowCpp]:<https://crowcpp.org/master/>
[AsioCpp]:<https://think-async.com/Asio/>
[W3C WebDriver]:<https://www.w3.org/TR/webdriver/>
[Agilitest]:<https://fr.agilitest.com/>


