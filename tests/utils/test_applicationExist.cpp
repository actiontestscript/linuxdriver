#include <catch2/catch_test_macros.hpp>
#include "../../include/utils/utils.hpp"

TEST_CASE("Test applicationExist method", "[Utils_applicationsExist_01]") {
    REQUIRE(ld::Utils::applicationExist("ls"));
}

TEST_CASE("Test applicationExist method", "[Utils_applicationsExist_02]") {
    REQUIRE_FALSE(ld::Utils::applicationExist("non_existent_application"));
}

TEST_CASE("Test applicationExist method with empty appName", "[Utils_applicationsExist_03]") {
    REQUIRE_FALSE(ld::Utils::applicationExist(""));
}

TEST_CASE("Test applicationExist method with invalid appName", "[Utils_applicationsExist_04]") {
    REQUIRE_FALSE(ld::Utils::applicationExist("foo-bar"));
}

TEST_CASE("Test applicationExist method with valid appName with absolute path", "[Utils_applicationsExist_05]") {
    REQUIRE(ld::Utils::applicationExist("/bin/ls"));
}

TEST_CASE("Test applicationExist method with valid appName with relative path", "[Utils_applicationsExist_06]") {
    REQUIRE(ld::Utils::applicationExist("./utils_tests"));
}


