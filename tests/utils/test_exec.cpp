#include <catch2/catch_test_macros.hpp>
#include "../../include/utils/utils.hpp"

TEST_CASE("Test exec method", "[Utils]") {
    std::string result;
    REQUIRE(ld::Utils::exec("echo Hello", result));
    REQUIRE(result == "Hello\n");
}