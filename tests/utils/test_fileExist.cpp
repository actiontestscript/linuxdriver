#include <catch2/catch_test_macros.hpp>
#include "../../include/utils/utils.hpp"

TEST_CASE("Test Utils::fileExist", "[Utils]") {
    REQUIRE(ld::Utils::fileExist("/tmp/test.txt") == false);
    REQUIRE(ld::Utils::fileExist("/etc/passwd") == true);
}