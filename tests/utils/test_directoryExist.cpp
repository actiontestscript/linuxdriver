#include <catch2/catch_test_macros.hpp>
#include "../../include/utils/utils.hpp"

TEST_CASE("Test Utils::directoryExist", "[Utils]") {
    REQUIRE(ld::Utils::directoryExist("/tmp/test") == false);
    REQUIRE(ld::Utils::directoryExist("/etc") == true);
}
