cmake_minimum_required(VERSION 3.22)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

project(utils_tests)

# Add Catch2
find_package(Catch2 REQUIRED)
#include_directories(PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../external/CrowCpp/include)
#include_directories(PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../external/asio/asio/include)




# Add test executable
add_executable(${PROJECT_NAME} test_main.cpp)


target_include_directories(${PROJECT_NAME}
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../src/lineCmdOption
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../src/crowSettings
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../src/W3C/driverStatus
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../src/W3C/session
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../src/W3C/capabilities
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../src/W3C/timeouts
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../src/W3C/navigate
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../src/utils
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../src/drivers
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../src/graphicInterface
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../src/ats/recorder
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../external/amf3-cpp/include
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../external/CrowCpp/external/asio/asio/include
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../external/CrowCpp/include  # Ajoutez cette ligne
)

# Link main project libraries and dependencies
add_subdirectory(.. ${CMAKE_CURRENT_BINARY_DIR}/linuxdriver)
target_link_libraries(${PROJECT_NAME} PRIVATE linuxdriver_deps)

#catch2
target_link_libraries(${PROJECT_NAME} PRIVATE Catch2::Catch2)

target_include_directories(${PROJECT_NAME} PRIVATE "../src/utils")

# CrowCpp
#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../external/CrowCpp ${CMAKE_CURRENT_BINARY_DIR}/CrowCpp)
#target_link_libraries(utils_tests PUBLIC Crow::Crow)

enable_testing()
add_test(NAME ${PROJECT_NAME} COMMAND utils_tests)

