#! /bin/sh
PARAM=$1
CURRENT_PATH=$(pwd)

cd external/amf3-cpp || exit
if [ ! -d "lib" ]; then
  mkdir  lib
fi
cd lib || exit

if [ "$PARAM" = "debug" ]; then
  echo "Debug mode for amf3-cpp"
  cmake -S ../ -B ./ -DCMAKE_BUILD_TYPE=Debug
else
  cmake -S ../ -B ./ -DCMAKE_BUILD_TYPE=Release
fi
make -j$(nproc)

cd "$CURRENT_PATH" || exit
if [ ! -d "external/opencv/build" ]; then
  mkdir -p external/opencv/build
  cd "$CURRENT_PATH" || exit
fi

if [ ! -d "external/opencv/install" ]; then
  mkdir -p external/opencv/install
fi

cd external/opencv/build
cmake -S ../ -B ./ -DBUILD_SHARED_LIBS=OFF -DCMAKE_INSTALL_PREFIX=../install -DWITH_ITT=OFF -DWITH_CAROTENE=OFF -DBUILD_ZLIB=ON -DWITH_JPEG=OFF -DWITH_JPEG2000=OFF -DWITH_PNG=ON -DWITH_WEBP=ON -DWITH_OPENJPEG=OFF -DWITH_TIFF=OFF -DWITH_JASPER=OFF -DWITH_OPENEXR=OFF -DBUILD_PNG=ON -DBUILD_WEBP=ON -DWITH_IPP=OFF -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTS=OFF -DBUILD_PERF_TESTS=OFF -DBUILD_EXAMPLES=OFF

make -j$(nproc)
make install

cd "$CURRENT_PATH" || exit
if [ ! -d "out" ]; then
  mkdir -p out/build
fi
cd out/build || exit
pwd
if [ "$PARAM" = "debug" ]; then
  echo "Debug mode for linuxdriver"
  cmake -DCMAKE_BUILD_TYPE=Debug -S ../../ -B ./
else
  cmake  -S ../../ -B ./ -DCMAKE_BUILD_TYPE=Release
fi
