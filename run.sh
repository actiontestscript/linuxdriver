#! /bin/sh
if [  -d "$HOME/.actiontestscript/drivers/" ]; then
cd $HOME/.actiontestscript/drivers/
./linuxdriver
else
  if [ -d "out/build" ]; then
    cd out/build
    ./linuxdriver
  fi
fi

