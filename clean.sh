#! /bin/sh
CURRENT_PATH=$(pwd)

cd "$CURRENT_PATH"
if [ -d "external/opencv/build" ]; then
  cd external/opencv/build
  find . -delete
fi
cd "$CURRENT_PATH"
if [ -d "external/opencv/install" ]; then
  cd external/opencv/install
  find . -delete
fi

cd "$CURRENT_PATH"
if [ -d "out/build" ]; then
  cd out/build
  find . -delete
fi

cd "$CURRENT_PATH"
if [ -d "external/amf3-cpp/lib" ]; then
  cd external/amf3-cpp/lib
  find . -delete
fi

