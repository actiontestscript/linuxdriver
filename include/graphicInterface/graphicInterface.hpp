#ifndef LINUXDRIVER_GRAPHICINTERFACE_HPP
#define LINUXDRIVER_GRAPHICINTERFACE_HPP
#include "../../apps/config.hpp"
#include <string>
#include <vector>
#include <iostream>

namespace ld{

    class GraphicInterface{
    private :

    protected:
       // std::string m_screenHeight;                              /*!< Screen height */
       // std::string m_screenWidth;                               /*!< Screen width */

    public:
        virtual bool createWindow() = 0;
        virtual std::vector<uint8_t> screenShot(const int& x, const int& y, const int& width, const int& height)=0;
    };

}
#endif //LINUXDRIVER_GRAPHICINTERFACE_HPP
