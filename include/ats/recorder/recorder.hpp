#ifndef LINUXDRIVER_RECORDER_HPP
#define LINUXDRIVER_RECORDER_HPP

#include <fstream>
#include "../../W3C/session/session.hpp"
#include "../../crowSettings/crowJsonResponse.hpp"
#include "../../../include/ats/recorder/desktopResponse.hpp"
#include "../../utils/httpRequest.hpp"

using json = nlohmann::json;

namespace ld {
    /*! @brief The class Recorder
     * @details This class is used recorder the actions of web browser. This class is abstract.
     */
    class Recorder {
    protected:
        Session *m_session{}; /*< Session object */
        CrowJsonResponse m_crowJsonResponse; /*< CrowJsonResponse object */
        std::string m_sessionId{}; /*< Session id */
        bool m_recorderOld{false}; /*< Recorder old */
        char m_postDataDelimiter{'\n'}; /*< Post data delimiter */
        std::string m_browserSessionId{}; /*< Browser session id */
        std::string m_browserScreenShotUrl{}; /*< Browser screen shot url */
        bool m_browserHeadless{false}; /*< Browser headless */

        /*!
         * @brief _parseJson Parse the json
         * @details Parse the json to get the data of the recorder start
         * @param req <crow::request>& The request
         * @return True if the json is parsed, false otherwise
         */
        virtual bool _parseJson(const crow::request &req) = 0;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param req <crow::request>& The request
         * @return True if the post data is parsed, false otherwise
         */
        virtual bool _parsePostData(const crow::request &req) = 0;

        /*!
         * @brief _jsonIsValid Check if the json is valid
         * @details Check if the json is valid
         * @param req <crow::request>& The request
         * @param res <crow::response>& The response
         * @return True if the json is valid, false otherwise
         */
        virtual bool _jsonIsValid(const crow::request &req, crow::response &res) = 0;

        /*!
         * @brief _jsonParseInt Parse the json to get an int
         * @details Parse the json to get an int
         * @param value <json>& value The json
         * @param key <std::string>& key The key
         * @param data int& The data
         */
        static void _jsonParseInt(const json &value, const std::string &key, int &data);

        /*!
         * @brief _jsonParseDouble Parse the json to get a double
         * @details Parse the json to get a double
         * @param value <json>& The json
         * @param key <std::string>& The key
         * @param data double& The data
         */
        static void _jsonParseDouble(const json &value, const std::string &key, double &data);

        /*!
         * @brief _jsonParseLong Parse the json to get a long
         * @details Parse the json to get a long
         * @param value <json>& The json
         * @param key <std::string>& The key
         * @param data long& The data
         */
        static void _jsonParseLong(const json &value, const std::string &key, long &data);

        /*!
         * @brief _jsonParseString Parse the json to get a string
         * @details Parse the json to get a string
         * @param value <json>& The json
         * @param key <std::string>& The key
         * @param data std::string& The data
         */
        static void _jsonParseString(const json &value, const std::string &key, std::string &data);

        /*!
         * @brief _jsonParseBool Parse the json to get a bool
         * @details Parse the json to get a bool
         * @param value <json>& The json
         * @param key <std::string>& The key
         * @param data bool& The data
         */
        static void _jsonParseBool(const json &value, const std::string &key, bool &data);

        /*!
         * @brief _strParseInt Parse the string to get an int
         * @details Parse the string to get an int
         * @param strResearch std::string& The string
         * @param delimiter char& The delimiter
         * @param defaultValue int& The default value
         * @param data int& The data
         */
        static void _strParseInt(std::string &strResearch, const char &delimiter, const int &defaultValue, int &data);

        /*!
         * @brief _strParseDouble Parse the string to get a double
         * @details Parse the string to get a double
         * @param strResearch std::string& The string
         * @param delimiter char& The delimiter
         * @param defaultValue double& The default value
         * @param data double& The data
         */
        static void _strParseDouble(std::string &strResearch, const char &delimiter, const double &defaultValue, double &data);

        /*!
         * @brief _strParseLong Parse the string to get a long
         * @details Parse the string to get a long
         * @param strResearch std::string& The string
         * @param delimiter char& The delimiter
         * @param defaultValue long& defaultValue The default value
         * @param data long& The data
         */
        static void _strParseLong(std::string &strResearch, const char &delimiter, const long &defaultValue, long &data);

        /*!
         * @brief _strParseString Parse the string to get a string
         * @details Parse the string to get a string
         * @param strResearch std::string& The string
         * @param delimiter char& The delimiter
         * @param defaultValue std::string& The default value
         * @param data std::string& The data
         */
        static void _strParseString(std::string &strResearch, const char &delimiter, const std::string &defaultValue,
                                    std::string &data);

        /*!
         * @brief _strParseBool Parse the string to get a bool
         * @details Parse the string to get a bool
         * @param strResearch std::string& The string
         * @param delimiter char& delimiter The delimiter
         * @param defaultValue bool& The default value
         * @param data bool& The data
         */
        static void _strParseBool(std::string &strResearch, const char &delimiter, const bool &defaultValue, bool &data);

    public:
        const std::string API_SESSION = "api-session";

        /*!
         * @brief The constructor of the class Recorder
         * @details This constructor is used to initialize the session object.
         */
        Recorder() { m_session = Session::getInstance(); }

        /*!
         * @brief The destructor of the class Recorder
         * @details This destructor is used to delete the session object.
         */
        virtual ~Recorder() = default;

        /*!
         * @brief The method execute
         * @details This method is used to execute the recorder. This method is abstract.
         * @param req <crow::request>&req The request object
         * @param res <crow::response>&res The response object
         * @return <bool> true if the recorder is executed, false otherwise
         */
        virtual bool execute(const crow::request &req, crow::response &res) =0;

        /*!
         * @brief setSessionId Set the session id
         * @details Set the session id
         * @param sessionId <std::string>& sessionId The session id
         */
        void setSessionId(const std::string &sessionId) {
            m_sessionId = sessionId;
        }

        /*!
         * @brief setRecorderOld Set the recorder old
         * @details Set the recorder old ats version
         * @param isOld bool True if the recorder is old, false otherwise
         */
        void setRecorderOld(const bool isOld) {
            m_recorderOld = isOld;
        }

        void setBrowserInfo(const json &value);

        void setBrowserInfoOld(std::string value);

        /*!
         * @brief flushFile Flush the file
         * @details Flush amf data into the file
         * @param filename <std::string>& filename The filename
         * @param data <amf::v8>& data The data
         */
        static void flushFile(const std::string &filename, const v8 &data);

        /*!
         * @brief flushVisualReport Flush the visual report
         * @details Flush the visual report
         * @param uuid <std::string>& uuid The uuid
         */
        void flushVisualReport(const std::string &uuid) const;

        /*!
         * @brief flushVisualAction Flush the visual action
         * @details Flush the visual action
         * @param uuid <std::string>& uuid The uuid
         */
        void flushVisualAction(const std::string &uuid) const;

        /*!
         * @brief flushReportSummary Flush the report summary
         * @details Flush the report summary
         * @param uuid <std::string>& uuid The uuid
         */
        void flushReportSummary(const std::string &uuid) const;

        /*!
         * @brief flushReportSummary Flush the report summary
         * @details Flush the report summary
         * @param res crow::response& The response
         */
        void getResponse(crow::response &res) const;

        void addImage(VisualAction &objVisual) const;

        void getWebDriverXY(const std::string &webDriverUrlScreenShot, double &offsetX, double &offsetY,
                            const bool &isHeadless) const;

        void setOffsetWebDriver(const std::string &webDriverUrlScreenShot, Session::RecorderInfo &recorderInfo,
                                const bool &isHeadless) const;
    };
}
#endif //LINUXDRIVER_RECORDER_HPP