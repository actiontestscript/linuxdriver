#ifndef LINUXDRIVER_VISUALREPORT_HPP
#define LINUXDRIVER_VISUALREPORT_HPP
#include "visualAction.hpp"
#include "../../W3C/capabilities/cap_os.hpp"

namespace ld{
    //class VisualReport : public VisualAction{
    class VisualReport {
    private:
        AmfObject amf_visualReport= AmfObject("com.ats.recorder.VisualReport", false, false);
        Os m_osInfo{};
    public:
        VisualReport()= default;
        VisualReport(const std::string& id, const std::string& package, const std::string& description, const std::string& memo, const std::string& author, const std::string& groups, const std::string& prereq, const std::string& externalId, int quality, const std::string& started, const std::string& modifiedAt, const std::string& modifiedBy);

        std::string author{};
        std::string description{};
        std::string memo{};
        std::string modifiedAt{};
        std::string modifiedBy{};
        std::string started{};
        std::string groups{};
        std::string id{};
        std::string script{};
        std::string prerequisite{};
        int quality{3};
        long cpuSpeed{};
        long totalMemory{};
        int cpuCount{};
        std::string osInfo{};
        std::string externalId{};

        // Accesseur pour obtenir l'objet AmfObject.
        [[nodiscard]] const amf::AmfObject& getAmfVisualReport() const { return amf_visualReport; }
        void setAmfVisualReport(amf::AmfObject& obj) const;
        void initAmfVisualReport();


    };
}
#endif //LINUXDRIVER_VISUALREPORT_HPP
