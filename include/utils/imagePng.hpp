#ifndef LINUXDRIVER_IMAGEPNG_HPP
#define LINUXDRIVER_IMAGEPNG_HPP

#include "../../../apps/config.hpp"
#include <opencv2/opencv.hpp>
#include <vector>
#include <cstdint>
#include <iostream>

namespace ld {

class ImagePng {
private:
    int m_colorType{CV_8UC3}; // Default to 3 channels (RGB)

public:
    ImagePng() : data(), width(0), height(0) {}

    cv::Mat data; // OpenCV matrix to store image data
    int width{0};
    int height{0};

    static void createEmptyPNGImage(ImagePng& img, int w, int h) {
        img.width = w;
        img.height = h;
        img.data = cv::Mat(h, w, CV_8UC4, cv::Scalar(0, 0, 0, 255)); // Create a blank RGBA image
    }

    std::vector<unsigned char> pngImageToBytes(const ImagePng& img) {
        std::vector<unsigned char> output;
        if (img.data.empty()) return output;

        std::vector<int> compression_params = {cv::IMWRITE_PNG_COMPRESSION, 3}; // Compression level
        cv::imencode(".png", img.data, output, compression_params);

        return output;
    }

    std::vector<unsigned char> pngImageToBytes(ImagePng& img, const int& w, const int& h) {
        if (img.data.empty()) createEmptyPNGImage(img, w, h);
        return pngImageToBytes(img);
    }

    void setPixelColor(const int& x, const int& y, const int& red = 0, const int& green = 0, const int& blue = 0, const int& alpha = 255) {
        if (x >= 0 && x < width && y >= 0 && y < height) {
            auto& pixel = data.at<cv::Vec4b>(y, x);
            pixel[0] = blue;  // OpenCV uses BGR(A)
            pixel[1] = green;
            pixel[2] = red;
            pixel[3] = alpha;
        }
    }

    void setAllPixelColor(const int& red = 0, const int& green = 0, const int& blue = 0, const int& alpha = 255) {
        if (data.empty()) return;
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                setPixelColor(x, y, red, green, blue, alpha);
            }
        }
    }

    static std::vector<uint8_t> vectorToPng(const std::vector<uint8_t>& image_data, const int& width, const int& height, const int& bytes_per_line) {
        if (image_data.empty()) return {};

        cv::Mat img(height, width, CV_8UC3); // RGB image
        for (int y = 0; y < height; ++y) {
            memcpy(img.ptr(y), &image_data[y * bytes_per_line], bytes_per_line);
        }

        std::vector<uint8_t> output;
        std::vector<int> compression_params = {cv::IMWRITE_PNG_COMPRESSION, 3};
        cv::imencode(".png", img, output, compression_params);

        return output;
    }

    void setColorType(int colorType) {
        if (colorType == CV_8UC1 || colorType == CV_8UC3 || colorType == CV_8UC4) {
            m_colorType = colorType;
        } else {
            m_colorType = CV_8UC3; // Default to RGB
        }
    }

    int getColorType() const { return m_colorType; }
};

} // namespace ld

#endif // LINUXDRIVER_IMAGEPNG_HPP