#ifndef LINUXDRIVER_HTTPREQUEST_HPP
#define LINUXDRIVER_HTTPREQUEST_HPP
#include <httplib.h>
#include <regex>
#include "utils.hpp"
#include <nlohmann/json.hpp>
using json = nlohmann::json;


namespace ld{
    class HttpRequest{
    public:
        static bool parseUrl(const std::string &url, std::string &hostname, std::string &port, std::string &params){
            std::regex pattern(R"(^http[s]?:\/\/([\w\.-]+):(\d+)(/.*))");
            std::smatch match;
            if(std::regex_search(url, match, pattern)){
                hostname = match[1];
                port = match[2];
                params = match[3];
                return true;
            }
            return false;
        }

        static json fetchJsonFromUrl(const std::string& url){
            //return nullptr;
            std::string hostname{};
            std::string port{};
            std::string params{};
            if (!parseUrl(url, hostname, port, params) || hostname.empty() || port.empty()) return nullptr;

            int portInt = 0;
            if(Utils::isInt(port)) portInt = std::stoi(port);
            else return nullptr;

            int tryCount = 0 ;
            int maxTryCount = 3;
  //          int status=0;
            try{
                httplib::Client client(hostname, portInt);
                while(tryCount < maxTryCount){
                    auto result = client.Get(params);
                    if (!result || (result && result->status != 200)) {
//                        status =result->status;
                        tryCount++;
//                    LD_LOG_DEBUG << "httpRequest fetchJsonFromUrl Error : try count => "<<tryCount << std::endl;
                        continue;
                    }
                    return json::parse(result->body);
                }
/*
                auto result = client.Get(params);
                if (!result || (result && result->status != 200)) {
                    return nullptr;
                }
                return json::parse(result->body);
*/
            }
            catch(const std::exception& e){
                LD_LOG_ERROR << "httpRequest fetchJsonFromUrl Error : " << e.what() << std::endl;
                return nullptr;
            }

            LD_LOG_ERROR << "httpRequest fetchJsonFromUrl Error" << std::endl;
            return nullptr;
        }

        static bool getIsLinuxDriverRunning(const int& port){
            httplib::Client client("localhost", port);
            auto result = client.Get("/isLinuxDriver");
            if (!result) return false;
            if(result->status == 200) return true;
            return false;
        }

        static void getWebDriverGetWindowRect(const std::string& webDriverUrlScreenShot, double &x, double &y, double &width, double &height){
            if(webDriverUrlScreenShot.empty()){ return; }
/*            std::string hostname{};
            std::string port{};
            std::string params{};
            std::regex pattern(R"(^http[s]?:\/\/([\w\.-]+):(\d+)(/.*))");
            std::smatch match;
            if(std::regex_search(webDriverUrlScreenShot, match, pattern)){
                hostname = match[1];
                port = match[2];
                params = match[3];
            }
            std::regex patternScreenshot(R"(/screenshot)");
            std::string paramWindowRect = std::regex_replace(params, patternScreenshot, "/window/rect");
            if(hostname.empty() || port.empty()) return;
            int portInt = 0;
            if(Utils::isInt(port)) portInt = std::stoi(port);
            else return;

            httplib::Client client(hostname, portInt);
            auto result = client.Get(paramWindowRect);
            if (!result) {
                LD_LOG_ERROR << "httpRequest getScreenShot Error : Unable to take screenShot WebDriver." << std::endl;
                return;
            }
            std::string resultJson;
            json jsonBody = json::parse(result->body);
*/
            json jsonBody = fetchJsonFromUrl(webDriverUrlScreenShot);
            if (jsonBody == nullptr) return;
            if(jsonBody.find("value") != jsonBody.end()){
                const json& value = jsonBody["value"];
                if(value.find("height") != value.end() && value["height"].is_number())
                    height = (value["height"].get<int>());
                if(value.find("width") != value.end() && value["width"].is_number())
                    width = (value["width"].get<int>());
                if(value.find("x") != value.end() && value["x"].is_number())
                    x = (value["x"].get<int>());
                if(value.find("y") != value.end() && value["y"].is_number())
                    y = (value["y"].get<int>());
            }
        }


        static std::string getScreenShot(const std::string& webDriverUrlScreenShot){
            if(webDriverUrlScreenShot.empty()){ return ""; }
/*
            std::string hostname{};
            std::string port{};
            std::string params{};
            std::regex pattern(R"(^http[s]?:\/\/([\w\.-]+):(\d+)(/.*))");
            std::smatch match;
            if(std::regex_search(webDriverUrlScreenShot, match, pattern)){
                hostname = match[1];
                port = match[2];
                params = match[3];
            }

            if(hostname.empty() || port.empty()) return "";
            int portInt = 0;
            if(Utils::isInt(port)) portInt = std::stoi(port);
            else return "";

            httplib::Client client(hostname, portInt);

            auto result = client.Get(params);
            if (!result) {
                LD_LOG_ERROR << "httpRequest getScreenShot Error : Unable to take screenShot WebDriver." << std::endl;
            return "";
            }

            json jsonBody = json::parse(result->body);
  */
            json jsonBody = fetchJsonFromUrl(webDriverUrlScreenShot);
            if(jsonBody == nullptr) return "";
            std::string screenshot_base64 = jsonBody["value"];
            if(screenshot_base64.empty()) return "";
            return screenshot_base64;
        }

        static std::vector<std::string> getWebDriverIdSession(const std::string& url){
            std::vector<std::string> result{};
            /*
            std::string hostname{};
            std::string port{};
            std::string params{};
            std::string sessionId{};
            std::regex pattern(R"(^http[s]?:\/\/([\w\.-]+):(\d+)(/.*))");
            std::smatch match;
            if(std::regex_search(url, match, pattern)){
                hostname = match[1];
                port = match[2];
                params = match[3];
            }

            if(hostname.empty() || port.empty()) return "";
            int portInt = 0;
            if(Utils::isInt(port)) portInt = std::stoi(port);
            else return "";

            httplib::Client client(hostname, portInt);

            auto result = client.Get(params);
            if (!result) {
                LD_LOG_ERROR << "httpRequest getWebDriverIdSession Error : Unable to take get id from  WebDriver." << std::endl;
                return "";
            }
            json jsonBody = json::parse(result->body);
  */
            json jsonBody = fetchJsonFromUrl(url);
            if(jsonBody == nullptr) return result;
            if(jsonBody.find("value") != jsonBody.end()) {
                for (const auto &session: jsonBody["value"]) {
                    if(session.find("id") != session.end() && session["id"].is_string()){
                        result.push_back(session["id"]);

                    }
                }
            }
            return result;
        }
    };
}
#endif //LINUXDRIVER_HTTPREQUEST_HPP
